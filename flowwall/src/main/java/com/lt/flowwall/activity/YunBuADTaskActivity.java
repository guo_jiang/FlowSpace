package com.lt.flowwall.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bytedance.sdk.openadsdk.AdSlot;
import com.bytedance.sdk.openadsdk.DislikeInfo;
import com.bytedance.sdk.openadsdk.FilterWord;
import com.bytedance.sdk.openadsdk.PersonalizationPrompt;
import com.bytedance.sdk.openadsdk.TTAdConstant;
import com.bytedance.sdk.openadsdk.TTAdDislike;
import com.bytedance.sdk.openadsdk.TTAdManager;
import com.bytedance.sdk.openadsdk.TTAdNative;
import com.bytedance.sdk.openadsdk.TTAppDownloadListener;
import com.bytedance.sdk.openadsdk.TTNativeExpressAd;
import com.kwad.sdk.api.KsAdSDK;
import com.kwad.sdk.api.KsInterstitialAd;
import com.kwad.sdk.api.KsLoadManager;
import com.kwad.sdk.api.KsScene;
import com.kwad.sdk.api.KsVideoPlayConfig;
import com.kwad.sdk.api.SdkConfig;
import com.lt.flowwall.R;
import com.lt.flowwall.app.Application;
import com.lt.flowwall.tools.DislikeDialog;
import com.lt.flowwall.tools.LuckPanLayout;
import com.lt.flowwall.tools.TTAdManagerHolder;
import com.lt.flowwall.utils.HttpUtils;
import com.lt.flowwall.utils.SessionSingleton;
import com.lt.flowwall.utils.Utils;
import com.lt.flowwall.yunbuimageload.AsyncImageLoader;
import com.lt.flowwall.yunbuimageload.FileCache;
import com.lt.flowwall.yunbuimageload.MemoryCache;
import com.qq.e.ads.interstitial2.UnifiedInterstitialAD;
import com.qq.e.ads.interstitial2.UnifiedInterstitialADListener;
import com.qq.e.ads.rewardvideo.RewardVideoAD;
import com.qq.e.comm.managers.GDTAdSdk;
import com.qq.e.comm.util.AdError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.os.Process;

public class YunBuADTaskActivity extends AppCompatActivity implements UnifiedInterstitialADListener {
    private Context context;
    public Dialog mLoading;

    private RelativeLayout rl_welfare_plaque_ad_background;
    private ImageView iv_welfare_plaque_ad_back;
    private TextView tv_welfare_plaque_ad_title;

    private ImageView iv_welfare_plaque_ad_luck_pan;
    private LinearLayout ll_welfare_plaque_ad_start;

    private TextView tv_welfare_plaque_ad_one_name, tv_welfare_plaque_ad_two_name, tv_welfare_plaque_ad_three_name, tv_welfare_plaque_ad_four_name;
    private ImageView iv_welfare_plaque_ad_one_image, iv_welfare_plaque_ad_two_image, iv_welfare_plaque_ad_three_image, iv_welfare_plaque_ad_four_image;

    private PopupWindow luckPopuo, luckPopuoOpen;

    private int CheckNum = 1;
    private int luckPrice = 0, MyScore = 0;
    private JSONArray luckPanArray;
    private LuckPanLayout yunbu_luckpan_layout;

    private String token, chanelUserAccount;
    private JSONArray recommendGoods;

    private AsyncImageLoader imageLoader;//异步组件

    private int checkedAd = 0;
    //快手
    private KsInterstitialAd mKsInterstitialAd;
    private KsVideoPlayConfig videoPlayConfig;
    private boolean isAdRequesting = false;
    //广点通
    private UnifiedInterstitialAD iad;
    //穿山甲
    private TTAdNative mTTAdNative;
    private TTNativeExpressAd mTTAd;
    private long startTime = 0;
    private boolean mHasShowDownloadActive = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        if (Build.VERSION.SDK_INT >= 21) {
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        setContentView(R.layout.activity_yun_bu_adtask);

        context = this;

        MemoryCache mcache = new MemoryCache();//内存缓存
        String paht = getApplicationContext().getFilesDir().getAbsolutePath();
        File cacheDir = new File(paht, "yunbucache");//缓存根目录
        FileCache fcache = new FileCache(context, cacheDir, "yunbuimage");//文件缓存
        imageLoader = new AsyncImageLoader(context, mcache, fcache);


        mLoading = Utils.createLoadingDialog(context, "正在加载......");
        mLoading.setCancelable(false);

        if (!SessionSingleton.getInstance().guangdiantong.equals("none")) {
            //广点通广告
            GDTAdSdk.init(context, SessionSingleton.getInstance().gdtId);
        }

        if (!SessionSingleton.getInstance().kuaishou.equals("none")) {
            String currentProcessName = getCurrentProcessName();
            if (currentProcessName.equals(getPackageName())) {
                // 建议只在需要的进程初始化SDK即可，如主进程
                KsAdSDK.init(context, new SdkConfig.Builder()
                        .appId(SessionSingleton.getInstance().ksId) // 测试aapId，请联系快手平台申请正式AppId，必填
                        .showNotification(true) // 是否展示下载通知栏
                        .debug(true)
                        .build());
            }
        }

        if (!SessionSingleton.getInstance().chuanshanjia.equals("none")) {
            //step1:初始化sdk
            TTAdManager ttAdManager = TTAdManagerHolder.get();
            //step2:(可选，强烈建议在合适的时机调用):申请部分权限，如read_phone_state,防止获取不了imei时候，下载类广告没有填充的问题。
            TTAdManagerHolder.get().requestPermissionIfNecessary(context);
            //step3:创建TTAdNative对象,用于调用广告请求接口
            mTTAdNative = ttAdManager.createAdNative(Application.getContext());
        }

        innitview();
        getAdLoadType();
        setlistener();
    }

    private String getCurrentProcessName() {
        int pid = Process.myPid();
        String currentProcessName = "";
        ActivityManager activityManager = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = activityManager.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo processInfo : runningAppProcesses) {
            if (pid == processInfo.pid) {
                currentProcessName = processInfo.processName;
            }
        }
        return currentProcessName;
    }

    private void setlistener() {
        iv_welfare_plaque_ad_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ll_welfare_plaque_ad_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAd();
            }
        });

        iv_welfare_plaque_ad_luck_pan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLuckPan();
            }
        });

        iv_welfare_plaque_ad_one_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (recommendGoods.getJSONObject(0).getString("channelName").equals("嘻趣")) {
                        Intent intent = new Intent(YunBuADTaskActivity.this, YunBuGameDetailsXiQuActivity.class);
                        intent.putExtra("ADID", recommendGoods.getJSONObject(0).getString("adId"));
                        intent.putExtra("KeFuQQ", recommendGoods.getJSONObject(0).getString("serverQQ"));
                        intent.putExtra("ShowFirst", SessionSingleton.getInstance().AccountSingle.getString("xiquShowFirstStep"));
                        startActivity(intent);

                    } else if (recommendGoods.getJSONObject(0).getString("channelName").equals("多游")) {
                        Intent intent = new Intent(YunBuADTaskActivity.this, YunBuGameDetailsDYActivity.class);
                        intent.putExtra("ADID", recommendGoods.getJSONObject(0).getString("adId"));
                        intent.putExtra("KeFuQQ", recommendGoods.getJSONObject(0).getString("serverQQ"));
                        intent.putExtra("ShowFirst", SessionSingleton.getInstance().AccountSingle.getString("duoyouShowFirstStep"));
                        startActivity(intent);
                    } else if (recommendGoods.getJSONObject(0).getString("channelName").equals("聚享游")) {
                        Intent intent = new Intent(YunBuADTaskActivity.this, YunBuGameDetailsJuXiangWanActivity.class);
                        intent.putExtra("ADID", recommendGoods.getJSONObject(0).getString("adId"));
                        intent.putExtra("KeFuQQ", recommendGoods.getJSONObject(0).getString("serverQQ"));
                        intent.putExtra("ShowFirst", SessionSingleton.getInstance().AccountSingle.getString("juxiangyouShowFirstStep"));
                        startActivity(intent);
                    } else if (recommendGoods.getJSONObject(0).getString("channelName").equals("闲玩")) {
                        Intent intent = new Intent(YunBuADTaskActivity.this, YunBuGameDetailsXianWanActivity.class);
                        intent.putExtra("ADID", recommendGoods.getJSONObject(0).getString("adId"));
                        intent.putExtra("KeFuQQ", recommendGoods.getJSONObject(0).getString("serverQQ"));
                        intent.putExtra("ShowFirst", SessionSingleton.getInstance().AccountSingle.getString("xianwanShowFirstStep"));
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        iv_welfare_plaque_ad_two_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (recommendGoods.getJSONObject(1).getString("channelName").equals("嘻趣")) {
                        Intent intent = new Intent(YunBuADTaskActivity.this, YunBuGameDetailsXiQuActivity.class);
                        intent.putExtra("ADID", recommendGoods.getJSONObject(1).getString("adId"));
                        intent.putExtra("KeFuQQ", recommendGoods.getJSONObject(1).getString("serverQQ"));
                        intent.putExtra("ShowFirst", SessionSingleton.getInstance().AccountSingle.getString("xiquShowFirstStep"));
                        startActivity(intent);

                    } else if (recommendGoods.getJSONObject(1).getString("channelName").equals("多游")) {
                        Intent intent = new Intent(YunBuADTaskActivity.this, YunBuGameDetailsDYActivity.class);
                        intent.putExtra("ADID", recommendGoods.getJSONObject(1).getString("adId"));
                        intent.putExtra("KeFuQQ", recommendGoods.getJSONObject(1).getString("serverQQ"));
                        intent.putExtra("ShowFirst", SessionSingleton.getInstance().AccountSingle.getString("duoyouShowFirstStep"));
                        startActivity(intent);
                    } else if (recommendGoods.getJSONObject(1).getString("channelName").equals("聚享游")) {
                        Intent intent = new Intent(YunBuADTaskActivity.this, YunBuGameDetailsJuXiangWanActivity.class);
                        intent.putExtra("ADID", recommendGoods.getJSONObject(1).getString("adId"));
                        intent.putExtra("KeFuQQ", recommendGoods.getJSONObject(1).getString("serverQQ"));
                        intent.putExtra("ShowFirst", SessionSingleton.getInstance().AccountSingle.getString("juxiangyouShowFirstStep"));
                        startActivity(intent);
                    } else if (recommendGoods.getJSONObject(1).getString("channelName").equals("闲玩")) {
                        Intent intent = new Intent(YunBuADTaskActivity.this, YunBuGameDetailsXianWanActivity.class);
                        intent.putExtra("ADID", recommendGoods.getJSONObject(1).getString("adId"));
                        intent.putExtra("KeFuQQ", recommendGoods.getJSONObject(1).getString("serverQQ"));
                        intent.putExtra("ShowFirst", SessionSingleton.getInstance().AccountSingle.getString("xianwanShowFirstStep"));
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        iv_welfare_plaque_ad_three_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (recommendGoods.getJSONObject(2).getString("channelName").equals("嘻趣")) {
                        Intent intent = new Intent(YunBuADTaskActivity.this, YunBuGameDetailsXiQuActivity.class);
                        intent.putExtra("ADID", recommendGoods.getJSONObject(2).getString("adId"));
                        intent.putExtra("KeFuQQ", recommendGoods.getJSONObject(2).getString("serverQQ"));
                        intent.putExtra("ShowFirst", SessionSingleton.getInstance().AccountSingle.getString("xiquShowFirstStep"));
                        startActivity(intent);

                    } else if (recommendGoods.getJSONObject(2).getString("channelName").equals("多游")) {
                        Intent intent = new Intent(YunBuADTaskActivity.this, YunBuGameDetailsDYActivity.class);
                        intent.putExtra("ADID", recommendGoods.getJSONObject(2).getString("adId"));
                        intent.putExtra("KeFuQQ", recommendGoods.getJSONObject(2).getString("serverQQ"));
                        intent.putExtra("ShowFirst", SessionSingleton.getInstance().AccountSingle.getString("duoyouShowFirstStep"));
                        startActivity(intent);
                    } else if (recommendGoods.getJSONObject(2).getString("channelName").equals("聚享游")) {
                        Intent intent = new Intent(YunBuADTaskActivity.this, YunBuGameDetailsJuXiangWanActivity.class);
                        intent.putExtra("ADID", recommendGoods.getJSONObject(2).getString("adId"));
                        intent.putExtra("KeFuQQ", recommendGoods.getJSONObject(2).getString("serverQQ"));
                        intent.putExtra("ShowFirst", SessionSingleton.getInstance().AccountSingle.getString("juxiangyouShowFirstStep"));
                        startActivity(intent);
                    } else if (recommendGoods.getJSONObject(2).getString("channelName").equals("闲玩")) {
                        Intent intent = new Intent(YunBuADTaskActivity.this, YunBuGameDetailsXianWanActivity.class);
                        intent.putExtra("ADID", recommendGoods.getJSONObject(2).getString("adId"));
                        intent.putExtra("KeFuQQ", recommendGoods.getJSONObject(2).getString("serverQQ"));
                        intent.putExtra("ShowFirst", SessionSingleton.getInstance().AccountSingle.getString("xianwanShowFirstStep"));
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        iv_welfare_plaque_ad_four_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (recommendGoods.getJSONObject(3).getString("channelName").equals("嘻趣")) {
                        Intent intent = new Intent(YunBuADTaskActivity.this, YunBuGameDetailsXiQuActivity.class);
                        intent.putExtra("ADID", recommendGoods.getJSONObject(3).getString("adId"));
                        intent.putExtra("KeFuQQ", recommendGoods.getJSONObject(3).getString("serverQQ"));
                        intent.putExtra("ShowFirst", SessionSingleton.getInstance().AccountSingle.getString("xiquShowFirstStep"));
                        startActivity(intent);

                    } else if (recommendGoods.getJSONObject(3).getString("channelName").equals("多游")) {
                        Intent intent = new Intent(YunBuADTaskActivity.this, YunBuGameDetailsDYActivity.class);
                        intent.putExtra("ADID", recommendGoods.getJSONObject(3).getString("adId"));
                        intent.putExtra("KeFuQQ", recommendGoods.getJSONObject(3).getString("serverQQ"));
                        intent.putExtra("ShowFirst", SessionSingleton.getInstance().AccountSingle.getString("duoyouShowFirstStep"));
                        startActivity(intent);
                    } else if (recommendGoods.getJSONObject(3).getString("channelName").equals("聚享游")) {
                        Intent intent = new Intent(YunBuADTaskActivity.this, YunBuGameDetailsJuXiangWanActivity.class);
                        intent.putExtra("ADID", recommendGoods.getJSONObject(3).getString("adId"));
                        intent.putExtra("KeFuQQ", recommendGoods.getJSONObject(3).getString("serverQQ"));
                        intent.putExtra("ShowFirst", SessionSingleton.getInstance().AccountSingle.getString("juxiangyouShowFirstStep"));
                        startActivity(intent);
                    } else if (recommendGoods.getJSONObject(3).getString("channelName").equals("闲玩")) {
                        Intent intent = new Intent(YunBuADTaskActivity.this, YunBuGameDetailsXianWanActivity.class);
                        intent.putExtra("ADID", recommendGoods.getJSONObject(3).getString("adId"));
                        intent.putExtra("KeFuQQ", recommendGoods.getJSONObject(3).getString("serverQQ"));
                        intent.putExtra("ShowFirst", SessionSingleton.getInstance().AccountSingle.getString("xianwanShowFirstStep"));
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void innitview() {
        rl_welfare_plaque_ad_background = findViewById(R.id.rl_welfare_plaque_ad_background);
        tv_welfare_plaque_ad_title = findViewById(R.id.tv_welfare_plaque_ad_title);
        iv_welfare_plaque_ad_back = findViewById(R.id.iv_welfare_plaque_ad_back);

        ll_welfare_plaque_ad_start = findViewById(R.id.ll_welfare_plaque_ad_start);

        iv_welfare_plaque_ad_luck_pan = findViewById(R.id.iv_welfare_plaque_ad_luck_pan);

        tv_welfare_plaque_ad_one_name = findViewById(R.id.tv_welfare_plaque_ad_one_name);
        tv_welfare_plaque_ad_two_name = findViewById(R.id.tv_welfare_plaque_ad_two_name);
        tv_welfare_plaque_ad_three_name = findViewById(R.id.tv_welfare_plaque_ad_three_name);
        tv_welfare_plaque_ad_four_name = findViewById(R.id.tv_welfare_plaque_ad_four_name);

        iv_welfare_plaque_ad_one_image = findViewById(R.id.iv_welfare_plaque_ad_one_image);
        iv_welfare_plaque_ad_two_image = findViewById(R.id.iv_welfare_plaque_ad_two_image);
        iv_welfare_plaque_ad_three_image = findViewById(R.id.iv_welfare_plaque_ad_three_image);
        iv_welfare_plaque_ad_four_image = findViewById(R.id.iv_welfare_plaque_ad_four_image);


        try {

            token = SessionSingleton.getInstance().AccountSingle.getString("token");
            chanelUserAccount = SessionSingleton.getInstance().AccountSingle.getString("chanelUserAccount");

            if (SessionSingleton.getInstance().hasStyleConfig == 1) {
                tv_welfare_plaque_ad_title.setTextColor(SessionSingleton.getInstance().mYBStyleConfig.getTitleTextColor());
                rl_welfare_plaque_ad_background.setBackgroundColor(SessionSingleton.getInstance().mYBStyleConfig.getTitleBackColor());


                if (SessionSingleton.getInstance().mYBStyleConfig.getTitleBackIcon() == 0) {
                    iv_welfare_plaque_ad_back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_back_black));
                } else {
                    iv_welfare_plaque_ad_back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_back_write));
                }
            }

            MyScore = SessionSingleton.getInstance().WelfareSingle.getInt("score");

            luckPanArray = SessionSingleton.getInstance().AccountSingle.getJSONArray("mainThreadAAAchoujiangProductSheet");
            luckPrice = luckPanArray.getJSONObject(0).getInt("onsumeTotal");

            recommendGoods = SessionSingleton.getInstance().AccountSingle.getJSONArray("gameRecommandArray");


            String imageurl1 = recommendGoods.getJSONObject(0).getString("logUrl");
            String imageurl2 = recommendGoods.getJSONObject(1).getString("logUrl");
            String imageurl3 = recommendGoods.getJSONObject(2).getString("logUrl");
            String imageurl4 = recommendGoods.getJSONObject(3).getString("logUrl");

            String title1 = recommendGoods.getJSONObject(0).getString("title");
            String title2 = recommendGoods.getJSONObject(1).getString("title");
            String title3 = recommendGoods.getJSONObject(2).getString("title");
            String title4 = recommendGoods.getJSONObject(3).getString("title");


            //异步加载图片，先从一级缓存、再二级缓存、最后网络获取图片
            Bitmap bmp1 = imageLoader.loadBitmap(iv_welfare_plaque_ad_one_image, imageurl1);
            if (bmp1 == null) {
                iv_welfare_plaque_ad_one_image.setImageResource(R.drawable.ic_load_iname);
            } else {
                iv_welfare_plaque_ad_one_image.setImageBitmap(bmp1);
            }

            //异步加载图片，先从一级缓存、再二级缓存、最后网络获取图片
            Bitmap bmp2 = imageLoader.loadBitmap(iv_welfare_plaque_ad_two_image, imageurl2);
            if (bmp2 == null) {
                iv_welfare_plaque_ad_two_image.setImageResource(R.drawable.ic_load_iname);
            } else {
                iv_welfare_plaque_ad_two_image.setImageBitmap(bmp2);
            }

            //异步加载图片，先从一级缓存、再二级缓存、最后网络获取图片
            Bitmap bmp3 = imageLoader.loadBitmap(iv_welfare_plaque_ad_three_image, imageurl3);
            if (bmp3 == null) {
                iv_welfare_plaque_ad_three_image.setImageResource(R.drawable.ic_load_iname);
            } else {
                iv_welfare_plaque_ad_three_image.setImageBitmap(bmp3);
            }

            //异步加载图片，先从一级缓存、再二级缓存、最后网络获取图片
            Bitmap bmp4 = imageLoader.loadBitmap(iv_welfare_plaque_ad_four_image, imageurl4);
            if (bmp4 == null) {
                iv_welfare_plaque_ad_four_image.setImageResource(R.drawable.ic_load_iname);
            } else {
                iv_welfare_plaque_ad_four_image.setImageBitmap(bmp4);
            }

            tv_welfare_plaque_ad_one_name.setText(title1);
            tv_welfare_plaque_ad_two_name.setText(title2);
            tv_welfare_plaque_ad_three_name.setText(title3);
            tv_welfare_plaque_ad_four_name.setText(title4);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    //签到 试玩  激励视频观看 激励视频观看下载  插屏下载  试玩充值
    private void TaskFinish(final String taskFinishType) {
        Map<String, String> params = new HashMap<>();
        params.put("chanelUserAccount", chanelUserAccount);
        params.put("token", token);
        params.put("type", taskFinishType);
        HttpUtils.doHttpReqeust("POST", SessionSingleton.getInstance().requestBaseUrl + "everyDayTaskGetApi?", params, new HttpUtils.StringCallback() {

            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject returnJSONObject = new JSONObject(response);
                    if (returnJSONObject.getString("status").equals("success")) {
                        SessionSingleton.getInstance().controlAdTime = returnJSONObject.getLong("description");

                    } else {
                        Utils.showToast(context, returnJSONObject.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mLoading.dismiss();
            }

            @Override
            public void onFaileure(int code, Exception e) {
                mLoading.dismiss();
                e.printStackTrace();
            }
        });
    }

    //luckpan
    private void LuckPanData() {
        Map<String, String> params = new HashMap<>();
        params.put("chanelUserAccount", chanelUserAccount);
        params.put("token", token);
        HttpUtils.doHttpReqeust("POST", SessionSingleton.getInstance().requestBaseUrl + "choujiangApi?", params, new HttpUtils.StringCallback() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject returnJSONObject = new JSONObject(response);
                    if (returnJSONObject.getString("status").equals("success")) {

                        CheckNum = returnJSONObject.getInt("rewardProductId");
                        yunbu_luckpan_layout.rotate(CheckNum - 1, 100);

                    } else {
                        Utils.showToast(context, returnJSONObject.getString("msg"));
                    }
                } catch (JSONException e) {
                    mLoading.dismiss();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFaileure(int code, Exception e) {
                e.printStackTrace();
            }
        });
    }

    //转盘
    private void showLuckPan() {
        LayoutInflater la = LayoutInflater.from(context);
        View contentView = la.inflate(R.layout.pop_yun_bu_luck_pan, null);//自定义布局
        TextView tv_pop_luck_pan_bill = contentView.findViewById(R.id.tv_pop_luck_pan_bill);
        yunbu_luckpan_layout = contentView.findViewById(R.id.yunbu_luckpan_layout);
        RelativeLayout yunbu_luckpan_start = contentView.findViewById(R.id.yunbu_luckpan_start);
        ImageView iv_pop_luckpan_close = contentView.findViewById(R.id.iv_pop_luckpan_close);
        TextView tv_yunbu_luckpan_price = contentView.findViewById(R.id.tv_yunbu_luckpan_price);


        tv_yunbu_luckpan_price.setText(luckPrice + "活跃值/次");


        yunbu_luckpan_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LuckPanData();
            }
        });

        tv_pop_luck_pan_bill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(YunBuADTaskActivity.this, YunBuIndianaBillActivity.class);
                startActivity(intent);
            }
        });

        yunbu_luckpan_layout.setAnimationEndListener(new LuckPanLayout.AnimationEndListener() {
            @Override
            public void endAnimation(int position) {
                showLuckPanOpen();
                MyScore = MyScore - luckPrice;
                try {
                    SessionSingleton.getInstance().WelfareSingle.put("score", String.valueOf(MyScore));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        iv_pop_luckpan_close.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                luckPopuo.dismiss();
            }
        });

        luckPopuo = new PopupWindow(contentView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);


        //设置PopupWindow的焦点
        luckPopuo.setFocusable(true);
        luckPopuo.setClippingEnabled(false);
        //点击PopupWindow之外的地方PopupWindow会消失
        luckPopuo.setOutsideTouchable(true);
        //showAtLocation(View parent, int gravity, int x, int y)：相对于父控件的位置（例如正中央Gravity.CENTER，下方Gravity.BOTTOM等），可以设置偏移或无偏移
        luckPopuo.showAtLocation(this.getWindow().getDecorView(), Gravity.CENTER, 0, 0);
        luckPopuo.update();
    }

    //dakai转盘
    private void showLuckPanOpen() {
        LayoutInflater la = LayoutInflater.from(context);
        View contentView = la.inflate(R.layout.pop_yun_bu_luck_pan_open, null);//自定义布局
        LinearLayout bg_pop_luck_pan_open_tyle = contentView.findViewById(R.id.bg_pop_luck_pan_open_tyle);
        ImageView iv_pop_luck_pan_open_tyle = contentView.findViewById(R.id.iv_pop_luck_pan_open_tyle);
        TextView tv_pop_luck_pan_open_money = contentView.findViewById(R.id.tv_pop_luck_pan_open_money);
        TextView tv_pop_luck_pan_open_ok = contentView.findViewById(R.id.tv_pop_luck_pan_open_ok);

        try {
            if (luckPanArray.getJSONObject(CheckNum - 1).getString("goodsName").contains("活跃")) {
                iv_pop_luck_pan_open_tyle.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_luck_pan_hyz));
                bg_pop_luck_pan_open_tyle.setBackground(getResources().getDrawable(R.mipmap.bg_pop_luck_pan_open_get));
                tv_pop_luck_pan_open_money.setText(luckPanArray.getJSONObject(CheckNum - 1).getString("goodsName"));

                int ontimesscore = Math.round(luckPanArray.getJSONObject(CheckNum - 1).getInt("rewardTotal"));
                MyScore = MyScore + ontimesscore;
                try {
                    SessionSingleton.getInstance().WelfareSingle.put("score", String.valueOf(MyScore));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (luckPanArray.getJSONObject(CheckNum - 1).getString("goodsName").contains("红包")) {
                iv_pop_luck_pan_open_tyle.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_luck_pan_hb));
                bg_pop_luck_pan_open_tyle.setBackground(getResources().getDrawable(R.mipmap.bg_pop_luck_pan_open_get));
                tv_pop_luck_pan_open_money.setText(luckPanArray.getJSONObject(CheckNum - 1).getString("goodsName"));
            } else if (luckPanArray.getJSONObject(CheckNum - 1).getString("goodsName").contains("谢谢参与")) {
                iv_pop_luck_pan_open_tyle.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_luck_pan_xx));
                bg_pop_luck_pan_open_tyle.setBackground(getResources().getDrawable(R.mipmap.bg_pop_luck_pan_open));
                tv_pop_luck_pan_open_money.setText("谢谢参与");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        tv_pop_luck_pan_open_ok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                luckPopuoOpen.dismiss();
            }
        });

        luckPopuoOpen = new PopupWindow(contentView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);


        //设置PopupWindow的焦点
        luckPopuoOpen.setFocusable(true);
        luckPopuoOpen.setClippingEnabled(false);
        //点击PopupWindow之外的地方PopupWindow会消失
        luckPopuoOpen.setOutsideTouchable(true);
        //showAtLocation(View parent, int gravity, int x, int y)：相对于父控件的位置（例如正中央Gravity.CENTER，下方Gravity.BOTTOM等），可以设置偏移或无偏移
        luckPopuoOpen.showAtLocation(this.getWindow().getDecorView(), Gravity.CENTER, 0, 0);
        luckPopuoOpen.update();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mTTAd != null) {
            mTTAd.destroy();
        }
        if (iad != null) {
            iad.close();
            iad.destroy();
        }

        if (mKsInterstitialAd != null) {
            mKsInterstitialAd = null;
        }
    }


    public void getAd() {

        if (checkedAd == 0) {
            loadExpressAd(SessionSingleton.getInstance().csjcp, 300, 450);
        } else if (checkedAd == 1) {
            iad = getIAD();
            iad.loadAD();
        } else {
            requestInterstitialAd();
        }
    }

    public void getAdLoadType() {
        try {
            if (mTTAd == null && iad == null && mKsInterstitialAd == null) {
                if (SessionSingleton.getInstance().chuanshanjia.equals("none") &&
                        SessionSingleton.getInstance().guangdiantong.equals("none") && SessionSingleton.getInstance().kuaishou.equals("none")) {
                    checkedAd = -1;
                    return;
                }

                if (!SessionSingleton.getInstance().chuanshanjia.equals("none") && !SessionSingleton.getInstance().guangdiantong.equals("none")
                        && !SessionSingleton.getInstance().kuaishou.equals("none")) {

                    if (checkedAd == 0) {
                        checkedAd = 1;
                    } else if (checkedAd == 1) {
                        checkedAd = 2;
                    } else if (checkedAd == 2) {
                        checkedAd = 0;
                    }

                } else {
                    if (SessionSingleton.getInstance().chuanshanjia.equals("yes") && SessionSingleton.getInstance().guangdiantong.equals("none")
                            && SessionSingleton.getInstance().kuaishou.equals("none")) {
                        checkedAd = 0;
                    } else if (SessionSingleton.getInstance().chuanshanjia.equals("none") && SessionSingleton.getInstance().guangdiantong.equals("yes")
                            && SessionSingleton.getInstance().kuaishou.equals("none")) {
                        checkedAd = 1;
                    } else if (SessionSingleton.getInstance().chuanshanjia.equals("none") && SessionSingleton.getInstance().guangdiantong.equals("none")
                            && SessionSingleton.getInstance().kuaishou.equals("yes")) {
                        checkedAd = 2;
                    } else if (!SessionSingleton.getInstance().chuanshanjia.equals("yes") && SessionSingleton.getInstance().guangdiantong.equals("yes")
                            && !SessionSingleton.getInstance().kuaishou.equals("none")) {
                        checkedAd = 2;
                    } else if (SessionSingleton.getInstance().chuanshanjia.equals("yes") && SessionSingleton.getInstance().guangdiantong.equals("yes")
                            && SessionSingleton.getInstance().kuaishou.equals("none")) {
                        if (checkedAd == 0) {
                            checkedAd = 1;
                        } else {
                            checkedAd = 0;
                        }
                    } else if (SessionSingleton.getInstance().chuanshanjia.equals("yes") && SessionSingleton.getInstance().guangdiantong.equals("none")
                            && SessionSingleton.getInstance().kuaishou.equals("yes")) {
                        if (checkedAd == 0) {
                            checkedAd = 2;
                        } else {
                            checkedAd = 0;
                        }
                    } else if (SessionSingleton.getInstance().chuanshanjia.equals("none") && SessionSingleton.getInstance().guangdiantong.equals("yes")
                            && SessionSingleton.getInstance().kuaishou.equals("yes")) {
                        if (checkedAd == 1) {
                            checkedAd = 2;
                        } else {
                            checkedAd = 1;
                        }
                    }

                }

            }
        } catch (Exception e) {
            String error = e.getMessage();
        }

    }

    //=======================================================快手插屏=============================================================================
    // 1.请求插屏广告，获取广告对象，InterstitialAd
    // 1.请求插屏广告，获取广告对象，InterstitialAd
    public void requestInterstitialAd() {
        if (isAdRequesting) {
            return;
        }
        isAdRequesting = true;
        mKsInterstitialAd = null;
        // 此为测试posId，请联系快手平台申请正式posId
        KsScene scene = new KsScene.Builder(Long.valueOf(SessionSingleton.getInstance().kscp)).build();
        KsAdSDK.getLoadManager().loadInterstitialAd(scene, new KsLoadManager.InterstitialAdListener() {
            @Override
            public void onError(int code, String msg) {
                Toast.makeText(context, "插屏广告请求失败" + code + msg, Toast.LENGTH_SHORT);
                isAdRequesting = false;

                //再次加载穿山甲
                if (SessionSingleton.getInstance().chuanshanjia.equals("none")) {
                    //再次加载广点通
                    if (SessionSingleton.getInstance().guangdiantong.equals("none")) {
                        //加载快手
                        if (SessionSingleton.getInstance().kuaishou.equals("none")) {
                            checkedAd = -1;
                            return;
                        } else {
                            checkedAd = 2;
                        }
                    } else {
                        checkedAd = 1;
                    }
                } else {
                    checkedAd = 0;
                }


                getAd();
            }

            @Override
            public void onRequestResult(int adNumber) {
                //Toast.makeText(context, "插屏广告请求填充个数 " + adNumber, Toast.LENGTH_SHORT);
            }


            @Override
            public void onInterstitialAdLoad(@Nullable List<KsInterstitialAd> adList) {
                isAdRequesting = false;
                if (adList != null && adList.size() > 0) {
                    mKsInterstitialAd = adList.get(0);
                    //Toast.makeText(context, "插屏广告请求成功", Toast.LENGTH_SHORT);

                    KsVideoPlayConfig videoPlayConfig = new KsVideoPlayConfig.Builder()
                            .videoSoundEnable(true)
                            .showLandscape(getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
                            .build();
                    showInterstitialAd(videoPlayConfig);
                }
            }
        });
    }

    private void showInterstitialAd(KsVideoPlayConfig videoPlayConfig) {
        if (mKsInterstitialAd != null) {
            mKsInterstitialAd.setAdInteractionListener(new KsInterstitialAd.AdInteractionListener() {
                @Override
                public void onAdClicked() {
                    // Toast.makeText(context, "插屏广告点击", Toast.LENGTH_SHORT);
                    getAdLoadType();
                    mLoading.show();
                    TaskFinish("insertAdLoad");
                }

                @Override
                public void onAdShow() {
                    //Toast.makeText(context, "插屏广告曝光", Toast.LENGTH_SHORT);
                }

                @Override
                public void onAdClosed() {
                    // Toast.makeText(context, "用户点击插屏关闭按钮", Toast.LENGTH_SHORT);
                    mKsInterstitialAd = null;
                    getAdLoadType();
                }

                @Override
                public void onPageDismiss() {
                    Log.i("TestInterstitialAd", "插屏广告关闭");
                    // Toast.makeText(context, "插屏广告关闭", Toast.LENGTH_SHORT);
                    mKsInterstitialAd = null;
                    //getAdLoadType();
                }

                @Override
                public void onVideoPlayError(int code, int extra) {
                    // Toast.makeText(context, "插屏广告播放出错", Toast.LENGTH_SHORT);
                    mKsInterstitialAd = null;
                    getAdLoadType();
                }

                @Override
                public void onVideoPlayEnd() {
                    //Toast.makeText(context, "插屏广告播放完成", Toast.LENGTH_SHORT);
                    mKsInterstitialAd = null;
                    getAdLoadType();
                }

                @Override
                public void onVideoPlayStart() {
                    //Toast.makeText(context, "插屏广告播放开始", Toast.LENGTH_SHORT);
                }

                @Override
                public void onSkippedAd() {
                    //Toast.makeText(context, "插屏广告播放跳过", Toast.LENGTH_SHORT);
                    mKsInterstitialAd = null;
                    getAdLoadType();
                }

            });
            mKsInterstitialAd.showInterstitialAd(this, videoPlayConfig);
        } else {
            // Toast.makeText(context, "暂无可用插屏广告，请等待缓存加载或者重新刷新", Toast.LENGTH_SHORT);
        }
    }

    //=======================================================穿山甲插屏=============================================================================
    private void loadExpressAd(String codeId, int expressViewWidth, int expressViewHeight) {
        //step4:创建广告请求参数AdSlot,具体参数含义参考文档

        AdSlot adSlot = new AdSlot.Builder()
                .setCodeId(codeId) //广告位id
                .setAdCount(1) //请求广告数量为1到3条
                .setExpressViewAcceptedSize(expressViewWidth, expressViewHeight) //期望模板广告view的size,单位dp
                .build();
        //step5:请求广告，对请求回调的广告作渲染处理

        mTTAdNative.loadInteractionExpressAd(adSlot, new TTAdNative.NativeExpressAdListener() {
            @Override
            public void onError(int code, String message) {
                // Utils.showToast(context,"load error : " + code + ", " + message);

                //再次加载广点通
                if (SessionSingleton.getInstance().guangdiantong.equals("none")) {
                    //展示失败加载快手
                    if (SessionSingleton.getInstance().kuaishou.equals("none")) {
                        //再次加载穿山甲
                        if (SessionSingleton.getInstance().chuanshanjia.equals("none")) {
                            checkedAd = -1;
                            return;
                        } else {
                            checkedAd = 0;
                        }
                    } else {
                        checkedAd = 2;
                    }
                } else {
                    checkedAd = 1;
                }

                getAd();
            }

            @Override

            public void onNativeExpressAdLoad(List<TTNativeExpressAd> ads) {
                if (ads == null || ads.size() == 0) {
                    return;
                }
                mTTAd = ads.get(0);
                bindAdListener(mTTAd);
                startTime = System.currentTimeMillis();
                mTTAd.render();
                //Utils.showToast(context, "load success !");
            }
        });
    }

    private void bindAdListener(final TTNativeExpressAd ad) {

        ad.setExpressInteractionListener(new TTNativeExpressAd.AdInteractionListener() {
            @Override

            public void onAdDismiss() {
                //Utils.showToast(context, "广告关闭");
                mTTAd = null;
                getAdLoadType();
            }

            @Override
            public void onAdClicked(View view, int type) {
                //Utils.showToast(context, "广告被点击");
            }

            @Override

            public void onAdShow(View view, int type) {
                // Utils.showToast(context, "广告展示");
            }

            @Override
            public void onRenderFail(View view, String msg, int code) {
                Log.e("ExpressView", "render fail:" + (System.currentTimeMillis() - startTime));
                // Utils.showToast(context, msg + " code:" + code);
            }

            @Override
            public void onRenderSuccess(View view, float width, float height) {
                Log.e("ExpressView", "render suc:" + (System.currentTimeMillis() - startTime));
                //返回view的宽高 单位 dp
                //Utils.showToast(context, "渲染成功");
                ad.showInteractionExpressAd(YunBuADTaskActivity.this);

            }
        });
        bindDislike(ad, false);

        if (ad.getInteractionType() != TTAdConstant.INTERACTION_TYPE_DOWNLOAD) {
            return;
        }
        ad.setDownloadListener(new TTAppDownloadListener() {
            @Override
            public void onIdle() {
                //Utils.showToast(context, "点击开始下载");
            }

            @Override
            public void onDownloadActive(long totalBytes, long currBytes, String fileName, String appName) {
                if (!mHasShowDownloadActive) {
                    mHasShowDownloadActive = true;
                    // Utils.showToast(context, "下载中，点击暂停");
                }
            }

            @Override
            public void onDownloadPaused(long totalBytes, long currBytes, String fileName, String appName) {
                //Utils.showToast(context, "下载暂停，点击继续");
            }

            @Override
            public void onDownloadFailed(long totalBytes, long currBytes, String fileName, String appName) {
                // Utils.showToast(context, "下载失败，点击重新下载");
            }

            @Override
            public void onInstalled(String fileName, String appName) {
                //Utils.showToast(context, "安装完成，点击图片打开");
                getAdLoadType();
                mLoading.show();
                TaskFinish("insertAdLoad");
            }

            @Override
            public void onDownloadFinished(long totalBytes, String fileName, String appName) {
                //Utils.showToast(context, "点击安装");
            }
        });
    }


    private void bindDislike(TTNativeExpressAd ad, boolean customStyle) {
        if (customStyle) {
            //使用自定义样式
            DislikeInfo dislikeInfo = ad.getDislikeInfo();
            if (dislikeInfo == null || dislikeInfo.getFilterWords() == null || dislikeInfo.getFilterWords().isEmpty()) {
                return;
            }
            final DislikeDialog dislikeDialog = new DislikeDialog(this, dislikeInfo);
            dislikeDialog.setOnDislikeItemClick(new DislikeDialog.OnDislikeItemClick() {
                @Override
                public void onItemClick(FilterWord filterWord) {
                    //屏蔽广告
                    //Utils.showToast(context,"点击 " + filterWord.getName());
                }
            });
            dislikeDialog.setOnPersonalizationPromptClick(new DislikeDialog.OnPersonalizationPromptClick() {
                @Override
                public void onClick(PersonalizationPrompt personalizationPrompt) {
                    //Utils.showToast(context,"点击了为什么看到此广告");
                }
            });
            ad.setDislikeDialog(dislikeDialog);
            return;
        }
        //使用默认模板中默认dislike弹出样式

        ad.setDislikeCallback(YunBuADTaskActivity.this, new TTAdDislike.DislikeInteractionCallback() {
            @Override
            public void onShow() {

            }

            @Override
            public void onSelected(int position, String value, boolean enforce) {
                //TToast.show(mContext, "反馈了 " + value);
                Utils.showToast(context, "\t\t\t\t\t\t\t感谢您的反馈!\t\t\t\t\t\t\n我们将为您带来更优质的广告体验");
                if (enforce) {
                    // Utils.showToast(context, "InteractionExpressActivity 模版插屏，穿山甲sdk强制将view关闭了 ");
                }
            }

            @Override
            public void onCancel() {
                //Utils.showToast(context, "点击取消 ");
            }

        });
    }

    //=======================================================广点通插屏=============================================================================
    @Override
    public void onADClicked() {
        getAdLoadType();
        mLoading.show();
        TaskFinish("insertAdLoad");
    }

    @Override
    public void onADClosed() {
        iad = null;
        getAdLoadType();
    }

    @Override
    public void onADExposure() {

    }

    @Override
    public void onADLeftApplication() {

    }

    @Override
    public void onADOpened() {

    }

    @Override
    public void onADReceive() {
        if (iad != null) {
            iad.show();
        } else {
            getAdLoadType();
        }
    }

    @Override
    public void onRenderFail() {
        iad = null;
        getAdLoadType();
    }

    @Override
    public void onRenderSuccess() {
        //Log.i("", "onRenderSuccess，建议在此回调后再调用展示方法");
    }

    @Override
    public void onNoAD(AdError adError) {
        iad = null;

        //加载快手
        if (SessionSingleton.getInstance().kuaishou.equals("none")) {
            //展示失败加载穿山甲
            if (SessionSingleton.getInstance().chuanshanjia.equals("none")) {
                //再次加载广点通
                if (SessionSingleton.getInstance().guangdiantong.equals("none")) {
                    checkedAd = -1;
                    return;
                } else {
                    checkedAd = 1;
                }

            } else {
                checkedAd = 0;
            }
        } else {
            checkedAd = 2;
        }

        getAd();
    }

    @Override
    public void onVideoCached() {


    }

    private UnifiedInterstitialAD getIAD() {
        if (this.iad != null) {
            iad.close();
            iad.destroy();
            iad = null;
        }
        iad = new UnifiedInterstitialAD(YunBuADTaskActivity.this, SessionSingleton.getInstance().gdtcp, this);
        return iad;
    }

}
