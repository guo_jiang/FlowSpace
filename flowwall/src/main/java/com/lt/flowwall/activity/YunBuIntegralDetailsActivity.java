package com.lt.flowwall.activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lt.flowwall.R;
import com.lt.flowwall.utils.HttpUtils;
import com.lt.flowwall.utils.SessionSingleton;
import com.lt.flowwall.utils.Utils;
import com.lt.flowwall.xPullRefresh.XListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class YunBuIntegralDetailsActivity extends AppCompatActivity {
    private Context context;
    public Dialog mLoading;

    private XListView xlv_score_bill;
    private ScoreBillAdapter adapter;
    private JSONArray scoreArray;

    private ImageView iv_integral_details_back;
    private TextView tv_integral_details_title;
    private RelativeLayout rl_integral_details_background;

    private TextView tv_integral_details_integral, tv_integral_details_get_integral;
    private TextView tv_integral_details_all, tv_integral_details_get, tv_integral_details_use;

    private String chanelUserAccount, Type, token;
    private int page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        if (Build.VERSION.SDK_INT >= 21) {
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        setContentView(R.layout.activity_integral_details);

        context = this;

        mLoading = Utils.createLoadingDialog(context, "正在加载......");
        mLoading.setCancelable(false);

        innitview();
        setlistener();
    }

    private void innitview() {
        iv_integral_details_back = findViewById(R.id.iv_integral_details_back);
        tv_integral_details_title = findViewById(R.id.tv_integral_details_title);
        rl_integral_details_background = findViewById(R.id.rl_integral_details_background);

        tv_integral_details_integral = findViewById(R.id.tv_integral_details_integral);
        tv_integral_details_get_integral = findViewById(R.id.tv_integral_details_get_integral);

        tv_integral_details_all = findViewById(R.id.tv_integral_details_all);
        tv_integral_details_get = findViewById(R.id.tv_integral_details_get);
        tv_integral_details_use = findViewById(R.id.tv_integral_details_use);

        xlv_score_bill = findViewById(R.id.xlv_score_bill);


        scoreArray = new JSONArray();

        try {
            token = SessionSingleton.getInstance().AccountSingle.getString("token");
            chanelUserAccount = SessionSingleton.getInstance().AccountSingle.getString("chanelUserAccount");

            if (SessionSingleton.getInstance().hasStyleConfig == 1) {
                tv_integral_details_title.setTextColor(SessionSingleton.getInstance().mYBStyleConfig.getTitleTextColor());
                rl_integral_details_background.setBackgroundColor(SessionSingleton.getInstance().mYBStyleConfig.getTitleBackColor());

                if (SessionSingleton.getInstance().mYBStyleConfig.getTitleBackIcon() == 0) {
                    iv_integral_details_back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_back_black));
                } else {
                    iv_integral_details_back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_back_write));
                }
            }

            tv_integral_details_integral.setText("当前活跃值：" + SessionSingleton.getInstance().WelfareSingle.getString("score"));

            mLoading.show();
            Type = "all";
            page = 1;
            DetailsListData(Type, page);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        xlv_score_bill.setPullRefreshEnable(true);
        xlv_score_bill.setPullLoadEnable(true);
        xlv_score_bill.setXListViewListener(new XListView.IXListViewListener() {

            @Override
            public void onRefresh() {
                page = 1;
                mLoading.show();
                DetailsListData(Type, page);
                Load();
            }

            @Override
            public void onLoadMore() {
                page = page + 1;
                mLoading.show();
                DetailsListData(Type, page);
                Load();
            }

        });
        adapter = new ScoreBillAdapter(context);
        xlv_score_bill.setAdapter(adapter);

    }

    private void Load() {
        xlv_score_bill.stopLoadMore();
        xlv_score_bill.stopRefresh();
    }

    private void setlistener() {
        iv_integral_details_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tv_integral_details_get_integral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(YunBuIntegralDetailsActivity.this, YunBuNavigateActivity.class);
                SessionSingleton.getInstance().backFragemntNum = 2;
                startActivity(intent);
            }
        });

        tv_integral_details_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_integral_details_all.setBackground(getResources().getDrawable(R.color.yunbu_lightgreen));
                tv_integral_details_get.setBackground(getResources().getDrawable(R.color.yunbu_write));
                tv_integral_details_use.setBackground(getResources().getDrawable(R.color.yunbu_write));

                tv_integral_details_all.setTextColor(getResources().getColor(R.color.yunbu_write));
                tv_integral_details_get.setTextColor(getResources().getColor(R.color.yunbu_textblack));
                tv_integral_details_use.setTextColor(getResources().getColor(R.color.yunbu_textblack));

                mLoading.show();
                Type = "all";
                page = 1;
                DetailsListData(Type, page);
            }
        });

        tv_integral_details_get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_integral_details_all.setBackground(getResources().getDrawable(R.color.yunbu_write));
                tv_integral_details_get.setBackground(getResources().getDrawable(R.color.yunbu_lightgreen));
                tv_integral_details_use.setBackground(getResources().getDrawable(R.color.yunbu_write));

                tv_integral_details_all.setTextColor(getResources().getColor(R.color.yunbu_textblack));
                tv_integral_details_get.setTextColor(getResources().getColor(R.color.yunbu_write));
                tv_integral_details_use.setTextColor(getResources().getColor(R.color.yunbu_textblack));

                mLoading.show();
                Type = "addtion";
                page = 1;
                DetailsListData(Type, page);
            }
        });

        tv_integral_details_use.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_integral_details_all.setBackground(getResources().getDrawable(R.color.yunbu_write));
                tv_integral_details_get.setBackground(getResources().getDrawable(R.color.yunbu_write));
                tv_integral_details_use.setBackground(getResources().getDrawable(R.color.yunbu_lightgreen));

                tv_integral_details_all.setTextColor(getResources().getColor(R.color.yunbu_textblack));
                tv_integral_details_get.setTextColor(getResources().getColor(R.color.yunbu_textblack));
                tv_integral_details_use.setTextColor(getResources().getColor(R.color.yunbu_write));

                mLoading.show();
                Type = "onsume";
                page = 1;
                DetailsListData(Type, page);
            }
        });
    }


    public class ScoreBillAdapter extends BaseAdapter {

        private LayoutInflater inflater;


        public ScoreBillAdapter(Context context) {
            inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return scoreArray.length();
        }

        @Override
        public Object getItem(int position) {

            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;

            if (convertView == null) {
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.item_yun_bu_indiana_details_item, null);
                holder.ll_indiana_details_item_bg = (LinearLayout) convertView.findViewById(R.id.ll_indiana_details_item_bg);
                holder.iv_indiana_details_item_head = (ImageView) convertView.findViewById(R.id.iv_indiana_details_item_head);
                holder.tv_indiana_details_item_title = (TextView) convertView.findViewById(R.id.tv_indiana_details_item_title);
                holder.tv_indiana_details_item_date = (TextView) convertView.findViewById(R.id.tv_indiana_details_item_date);
                holder.tv_indiana_details_item_times = (TextView) convertView.findViewById(R.id.tv_indiana_details_item_times);
                holder.tv_indiana_details_join_num = (TextView) convertView.findViewById(R.id.tv_indiana_details_join_num);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            try {
                JSONObject single = scoreArray.getJSONObject(position);

                holder.ll_indiana_details_item_bg.setVisibility(View.GONE);
                holder.tv_indiana_details_join_num.setVisibility(View.GONE);

                holder.tv_indiana_details_item_title.setText(single.getString("title"));
                holder.tv_indiana_details_item_date.setText(single.getString("datetime"));
                holder.tv_indiana_details_item_times.setText(single.getString("content"));
                if (single.getString("content").contains("+")) {
                    holder.tv_indiana_details_item_times.setTextColor(getResources().getColor(R.color.yunbu_green));
                } else {
                    holder.tv_indiana_details_item_times.setTextColor(getResources().getColor(R.color.yunbu_textchecked));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            return convertView;
        }


        class ViewHolder {
            TextView tv_indiana_details_item_title, tv_indiana_details_item_date, tv_indiana_details_item_times, tv_indiana_details_join_num;
            ImageView iv_indiana_details_item_head;
            LinearLayout ll_indiana_details_item_bg;
        }

    }


    private void DetailsListData(String type, final int pageindex) {
        Map<String, String> params = new HashMap<>();
        params.put("chanelUserAccount", chanelUserAccount);
        params.put("token", token);
        params.put("page", String.valueOf(pageindex));
        params.put("type", type);
        HttpUtils.doHttpReqeust("POST", SessionSingleton.getInstance().requestBaseUrl + "scoreChangeDetailsRecordApi?", params, new HttpUtils.StringCallback() {

            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject returnJSONObject = new JSONObject(response);
                    if (returnJSONObject.getString("status").equals("success")) {

                        JSONArray array = returnJSONObject.getJSONArray("data");
                        if (pageindex == 1) {
                            scoreArray = array;
                        } else {
                            for (int i = 0; i < array.length(); i++) {
                                scoreArray.put(array.getJSONObject(i));
                            }
                        }
                        adapter.notifyDataSetChanged();
                    } else {
                        Utils.showToast(context, returnJSONObject.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mLoading.dismiss();
            }

            @Override
            public void onFaileure(int code, Exception e) {
                mLoading.dismiss();
                e.printStackTrace();
            }
        });
    }
}
