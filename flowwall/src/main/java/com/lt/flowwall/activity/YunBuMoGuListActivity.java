package com.lt.flowwall.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bytedance.sdk.openadsdk.AdSlot;
import com.bytedance.sdk.openadsdk.DislikeInfo;
import com.bytedance.sdk.openadsdk.FilterWord;
import com.bytedance.sdk.openadsdk.PersonalizationPrompt;
import com.bytedance.sdk.openadsdk.TTAdConstant;
import com.bytedance.sdk.openadsdk.TTAdDislike;
import com.bytedance.sdk.openadsdk.TTAdManager;
import com.bytedance.sdk.openadsdk.TTAdNative;
import com.bytedance.sdk.openadsdk.TTAppDownloadListener;
import com.bytedance.sdk.openadsdk.TTNativeExpressAd;
import com.fendasz.moku.planet.entity.MokuOptions;
import com.fendasz.moku.planet.exception.MokuException;
import com.fendasz.moku.planet.helper.ApiDataHelper;
import com.fendasz.moku.planet.helper.MokuHelper;
import com.fendasz.moku.planet.source.bean.ClientSampleTaskData;
import com.fendasz.moku.planet.source.bean.ClientSampleTaskDataList;
import com.fendasz.moku.planet.utils.PhoneInfoUtils;
import com.kwad.sdk.api.KsAdSDK;
import com.kwad.sdk.api.KsInterstitialAd;
import com.kwad.sdk.api.KsLoadManager;
import com.kwad.sdk.api.KsScene;
import com.kwad.sdk.api.KsVideoPlayConfig;
import com.kwad.sdk.api.SdkConfig;
import com.lt.flowwall.R;
import com.lt.flowwall.app.Application;
import com.lt.flowwall.tools.DislikeDialog;
import com.lt.flowwall.tools.TTAdManagerHolder;
import com.lt.flowwall.utils.SessionSingleton;
import com.lt.flowwall.utils.Utils;
import com.lt.flowwall.xPullRefresh.XListView;
import com.lt.flowwall.yunbuimageload.AsyncImageLoader;
import com.lt.flowwall.yunbuimageload.FileCache;
import com.lt.flowwall.yunbuimageload.MemoryCache;
import com.qq.e.ads.interstitial2.UnifiedInterstitialAD;
import com.qq.e.ads.interstitial2.UnifiedInterstitialADListener;
import com.qq.e.comm.managers.GDTAdSdk;
import com.qq.e.comm.util.AdError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class YunBuMoGuListActivity extends AppCompatActivity implements UnifiedInterstitialADListener {
    private Context context;
    public Dialog mLoading;
    private XListView xlv_mogu;
    private ImageView iv_mogu_back;
    private TextView tv_mogu_title;
    private RelativeLayout rl_mogu_title_background;

    private TextView tv_mogu_title_ty1, tv_mogu_title_ty2, tv_mogu_title_ty3, tv_mogu_title_ty4, tv_mogu_title_ty5;
    private TextView tv_mogu_title_ty1_zhishiqi, tv_mogu_title_ty2_zhishiqi, tv_mogu_title_ty3_zhishiqi, tv_mogu_title_ty4_zhishiqi, tv_mogu_title_ty5_zhishiqi;

    ApiDataHelper mApiDataHelper = null;
    private int taskDataId;
    private List<ClientSampleTaskData> moguDataList;
    private List<ClientSampleTaskData> moguDataListSift;
    private List<ClientSampleTaskData> listsift;
    private MoGuListAdapter adapter;

    private String chanelUserAccount, imei1, oaid;
    private String moguStatus;
    private int page;

    private int checkedAd = 0;
    //快手
    private KsInterstitialAd mKsInterstitialAd;
    private KsVideoPlayConfig videoPlayConfig;
    private boolean isAdRequesting = false;
    //广点通
    private UnifiedInterstitialAD iad;
    //穿山甲
    private TTAdNative mTTAdNative;
    private TTNativeExpressAd mTTAd;
    private long startTime = 0;
    private boolean mHasShowDownloadActive = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        if (Build.VERSION.SDK_INT >= 21) {
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        setContentView(R.layout.activity_yun_bu_mo_gu_list);

        context = this;

        /*** 申请权限 * @param activity */
        MokuHelper.requestPermissions(YunBuMoGuListActivity.this);


        if (!SessionSingleton.getInstance().chuanshanjia.equals("none")) {
            //step1:初始化sdk
            TTAdManager ttAdManager = TTAdManagerHolder.get();
            //step2:(可选，强烈建议在合适的时机调用):申请部分权限，如read_phone_state,防止获取不了imei时候，下载类广告没有填充的问题。
            TTAdManagerHolder.get().requestPermissionIfNecessary(context);
            //step3:创建TTAdNative对象,用于调用广告请求接口
            mTTAdNative = ttAdManager.createAdNative(Application.getContext());
        }


        mLoading = Utils.createLoadingDialog(context, "正在加载......");
        mLoading.setCancelable(false);

        innitview();
        setlistener();
    }


    private void innitview() {
        xlv_mogu = findViewById(R.id.xlv_mogu);

        tv_mogu_title_ty1 = findViewById(R.id.tv_mogu_title_ty1);
        tv_mogu_title_ty2 = findViewById(R.id.tv_mogu_title_ty2);
        tv_mogu_title_ty3 = findViewById(R.id.tv_mogu_title_ty3);
        tv_mogu_title_ty4 = findViewById(R.id.tv_mogu_title_ty4);
        tv_mogu_title_ty5 = findViewById(R.id.tv_mogu_title_ty5);

        tv_mogu_title_ty1_zhishiqi = findViewById(R.id.tv_mogu_title_ty1_zhishiqi);
        tv_mogu_title_ty2_zhishiqi = findViewById(R.id.tv_mogu_title_ty2_zhishiqi);
        tv_mogu_title_ty3_zhishiqi = findViewById(R.id.tv_mogu_title_ty3_zhishiqi);
        tv_mogu_title_ty4_zhishiqi = findViewById(R.id.tv_mogu_title_ty4_zhishiqi);
        tv_mogu_title_ty5_zhishiqi = findViewById(R.id.tv_mogu_title_ty5_zhishiqi);

        iv_mogu_back = findViewById(R.id.iv_mogu_back);
        tv_mogu_title = findViewById(R.id.tv_mogu_title);
        rl_mogu_title_background = findViewById(R.id.rl_mogu_title_background);


        try {
            chanelUserAccount = SessionSingleton.getInstance().AccountSingle.getString("chanelUserAccount");
            imei1 = PhoneInfoUtils.getInstance().getPhoneImeiNum(context);
            oaid = SessionSingleton.getInstance().AccountSingle.getString("oaId");

            if (SessionSingleton.getInstance().hasStyleConfig == 1) {
                tv_mogu_title.setTextColor(SessionSingleton.getInstance().mYBStyleConfig.getTitleTextColor());
                rl_mogu_title_background.setBackgroundColor(SessionSingleton.getInstance().mYBStyleConfig.getTitleBackColor());


                if (SessionSingleton.getInstance().mYBStyleConfig.getTitleBackIcon() == 0) {
                    iv_mogu_back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_back_black));
                } else {
                    iv_mogu_back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_back_write));
                }
            }

            MokuOptions mokuOptions = new MokuOptions();
            //配置用户id
            mokuOptions.putString("userId", chanelUserAccount);
            //配置appId
            mokuOptions.putString("appId", SessionSingleton.getInstance().moguId);
            //配置appSecret
            mokuOptions.putString("appSecret", SessionSingleton.getInstance().moguSecret);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                //配置oaid
                mokuOptions.putString("oaid", oaid);
            } else {
                //配置imei
                mokuOptions.putString("imei", imei1);
            }


            //配置对接方式 ‐1:兼容老版本sdk对接 0:sdk对接 1:cpa弹窗对接 2:api对接
            mokuOptions.putInteger("cutInType", 2);

            try {
                MokuHelper.startSdk(getBaseContext(), mokuOptions);
            } catch (MokuException e) {
                e.printStackTrace();
            }

            //获取到ApiDataHelper的实例mApiDataHelper后，调用mApiDataHelper对象中的方法获取数据
            mApiDataHelper = ApiDataHelper.getInstance(context);


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (MokuException e) {
            e.printStackTrace();
        }


        page = 1;
        moguStatus = "全部";
        mLoading.show();
        getMoGuData(moguStatus, page);

        xlv_mogu.setPullRefreshEnable(true);
        xlv_mogu.setPullLoadEnable(true);
        xlv_mogu.setXListViewListener(new XListView.IXListViewListener() {

            @Override
            public void onRefresh() {
                page = 1;
                mLoading.show();
                moguDataList.clear();
                getMoGuData(moguStatus, page);

                Load();
            }

            @Override
            public void onLoadMore() {
                page = page + 1;
                mLoading.show();

                getMoGuData(moguStatus, page);
                Load();
            }

        });


        xlv_mogu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                taskDataId = moguDataList.get(position).getTaskDataId();
                if (SessionSingleton.getInstance().showInsertAd.equals("yes")) {
                    getAd();
                } else {
                    gomogu();
                }

            }
        });


    }

    private void Load() {
        xlv_mogu.stopLoadMore();
        xlv_mogu.stopRefresh();
    }


    private void setlistener() {
        iv_mogu_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tv_mogu_title_ty1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_mogu_title_ty1.setTextColor(getResources().getColor(R.color.yunbu_textblack));
                tv_mogu_title_ty2.setTextColor(getResources().getColor(R.color.yunbu_textgray2));
                tv_mogu_title_ty3.setTextColor(getResources().getColor(R.color.yunbu_textgray2));
                tv_mogu_title_ty4.setTextColor(getResources().getColor(R.color.yunbu_textgray2));

                tv_mogu_title_ty1.setTextSize(17);
                tv_mogu_title_ty2.setTextSize(14);
                tv_mogu_title_ty3.setTextSize(14);
                tv_mogu_title_ty4.setTextSize(14);
                tv_mogu_title_ty5.setTextSize(14);

                tv_mogu_title_ty1_zhishiqi.setVisibility(View.VISIBLE);
                tv_mogu_title_ty2_zhishiqi.setVisibility(View.GONE);
                tv_mogu_title_ty3_zhishiqi.setVisibility(View.GONE);
                tv_mogu_title_ty4_zhishiqi.setVisibility(View.GONE);
                tv_mogu_title_ty5_zhishiqi.setVisibility(View.GONE);

                moguStatus = "全部";
                page = 1;
                //快审  高额  简单 所有
                moguDataList.clear();
                mLoading.show();
                getMoGuData(moguStatus, page);

                adapter.notifyDataSetChanged();
            }
        });

        tv_mogu_title_ty2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_mogu_title_ty1.setTextColor(getResources().getColor(R.color.yunbu_textgray2));
                tv_mogu_title_ty2.setTextColor(getResources().getColor(R.color.yunbu_textblack));
                tv_mogu_title_ty3.setTextColor(getResources().getColor(R.color.yunbu_textgray2));
                tv_mogu_title_ty4.setTextColor(getResources().getColor(R.color.yunbu_textgray2));

                tv_mogu_title_ty1.setTextSize(14);
                tv_mogu_title_ty2.setTextSize(17);
                tv_mogu_title_ty3.setTextSize(14);
                tv_mogu_title_ty4.setTextSize(14);
                tv_mogu_title_ty5.setTextSize(14);

                tv_mogu_title_ty1_zhishiqi.setVisibility(View.GONE);
                tv_mogu_title_ty2_zhishiqi.setVisibility(View.VISIBLE);
                tv_mogu_title_ty3_zhishiqi.setVisibility(View.GONE);
                tv_mogu_title_ty4_zhishiqi.setVisibility(View.GONE);
                tv_mogu_title_ty5_zhishiqi.setVisibility(View.GONE);

                moguStatus = "截图";
                page = 1;
                moguDataList.clear();
                mLoading.show();
                getMoGuData(moguStatus, page);

                adapter.notifyDataSetChanged();


            }
        });

        tv_mogu_title_ty3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_mogu_title_ty1.setTextColor(getResources().getColor(R.color.yunbu_textgray2));
                tv_mogu_title_ty2.setTextColor(getResources().getColor(R.color.yunbu_textgray2));
                tv_mogu_title_ty3.setTextColor(getResources().getColor(R.color.yunbu_textblack));
                tv_mogu_title_ty4.setTextColor(getResources().getColor(R.color.yunbu_textgray2));

                tv_mogu_title_ty1.setTextSize(14);
                tv_mogu_title_ty2.setTextSize(14);
                tv_mogu_title_ty3.setTextSize(17);
                tv_mogu_title_ty4.setTextSize(14);
                tv_mogu_title_ty5.setTextSize(14);

                tv_mogu_title_ty1_zhishiqi.setVisibility(View.GONE);
                tv_mogu_title_ty2_zhishiqi.setVisibility(View.GONE);
                tv_mogu_title_ty3_zhishiqi.setVisibility(View.VISIBLE);
                tv_mogu_title_ty4_zhishiqi.setVisibility(View.GONE);
                tv_mogu_title_ty5_zhishiqi.setVisibility(View.GONE);

                moguStatus = "关键词";
                page = 1;
                moguDataList.clear();
                mLoading.show();
                getMoGuData(moguStatus, page);

            }
        });

        tv_mogu_title_ty4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_mogu_title_ty1.setTextColor(getResources().getColor(R.color.yunbu_textgray2));
                tv_mogu_title_ty2.setTextColor(getResources().getColor(R.color.yunbu_textgray2));
                tv_mogu_title_ty3.setTextColor(getResources().getColor(R.color.yunbu_textgray2));
                tv_mogu_title_ty4.setTextColor(getResources().getColor(R.color.yunbu_textblack));

                tv_mogu_title_ty1.setTextSize(14);
                tv_mogu_title_ty2.setTextSize(14);
                tv_mogu_title_ty3.setTextSize(14);
                tv_mogu_title_ty4.setTextSize(17);
                tv_mogu_title_ty5.setTextSize(14);

                tv_mogu_title_ty1_zhishiqi.setVisibility(View.GONE);
                tv_mogu_title_ty2_zhishiqi.setVisibility(View.GONE);
                tv_mogu_title_ty3_zhishiqi.setVisibility(View.GONE);
                tv_mogu_title_ty4_zhishiqi.setVisibility(View.VISIBLE);
                tv_mogu_title_ty5_zhishiqi.setVisibility(View.GONE);

                moguStatus = "评论";
                page = 1;
                moguDataList.clear();
                mLoading.show();
                getMoGuData(moguStatus, page);

                adapter.notifyDataSetChanged();


            }
        });

        tv_mogu_title_ty5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_mogu_title_ty1.setTextColor(getResources().getColor(R.color.yunbu_textgray2));
                tv_mogu_title_ty2.setTextColor(getResources().getColor(R.color.yunbu_textgray2));
                tv_mogu_title_ty3.setTextColor(getResources().getColor(R.color.yunbu_textgray2));
                tv_mogu_title_ty4.setTextColor(getResources().getColor(R.color.yunbu_textblack));

                tv_mogu_title_ty1.setTextSize(14);
                tv_mogu_title_ty2.setTextSize(14);
                tv_mogu_title_ty3.setTextSize(14);
                tv_mogu_title_ty4.setTextSize(14);
                tv_mogu_title_ty5.setTextSize(17);

                tv_mogu_title_ty1_zhishiqi.setVisibility(View.GONE);
                tv_mogu_title_ty2_zhishiqi.setVisibility(View.GONE);
                tv_mogu_title_ty3_zhishiqi.setVisibility(View.GONE);
                tv_mogu_title_ty4_zhishiqi.setVisibility(View.GONE);
                tv_mogu_title_ty5_zhishiqi.setVisibility(View.VISIBLE);

                moguStatus = "cpa";
                page = 1;
                moguDataList.clear();
                mLoading.show();
                getMoGuData(moguStatus, page);

                adapter.notifyDataSetChanged();

            }
        });
    }


    public class MoGuListAdapter extends BaseAdapter {
        private AsyncImageLoader imageLoader;//异步组件
        private LayoutInflater inflater;


        public MoGuListAdapter(Context context) {

            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            MemoryCache mcache = new MemoryCache();//内存缓存
            String paht = getApplicationContext().getFilesDir().getAbsolutePath();
            File cacheDir = new File(paht, "yunbucache");//缓存根目录
            FileCache fcache = new FileCache(context, cacheDir, "yunbuimage");//文件缓存
            imageLoader = new AsyncImageLoader(context, mcache, fcache);
        }

        @Override
        public int getCount() {
            return moguDataList.size();
        }

        @Override
        public Object getItem(int position) {

            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;

            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_yun_bu_mogu_list, null);
            holder.iv_item_mogu_head = (ImageView) convertView.findViewById(R.id.iv_item_mogu_head);
            holder.tv_item_mogu_title = (TextView) convertView.findViewById(R.id.tv_item_mogu_title);
            holder.tv_item_mogu_lable = (TextView) convertView.findViewById(R.id.tv_item_mogu_lable);
            holder.tv_item_mogu_desc = (TextView) convertView.findViewById(R.id.tv_item_mogu_desc);
            holder.tv_item_mogu_money = (TextView) convertView.findViewById(R.id.tv_item_mogu_money);
            holder.tv_item_mogu_surplus = (TextView) convertView.findViewById(R.id.tv_item_mogu_surplus);

            try {
                holder.tv_item_mogu_title.setText(moguDataList.get(position).getShowName());
                holder.tv_item_mogu_surplus.setText("剩余：" + moguDataList.get(position).getSurplusNum() + "个");
                if (moguDataList.get(position).getTagNameStr().get(0).getTagName().equals("")) {
                    holder.tv_item_mogu_lable.setVisibility(View.GONE);
                } else {
                    holder.tv_item_mogu_lable.setText(moguDataList.get(position).getTagNameStr().get(0).getTagName());
                }

                holder.tv_item_mogu_desc.setText(moguDataList.get(position).getDesc());
                holder.tv_item_mogu_money.setText(moguDataList.get(position).getShowMoney() + moguDataList.get(position).getCybermoneyName());

                //异步加载图片，先从一级缓存、再二级缓存、最后网络获取图片
                Bitmap bmp = imageLoader.loadBitmap(holder.iv_item_mogu_head, moguDataList.get(position).getIcon());
                if (bmp == null) {
                    holder.iv_item_mogu_head.setImageResource(R.drawable.ic_load_iname);
                } else {
                    holder.iv_item_mogu_head.setImageBitmap(bmp);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            return convertView;
        }


        class ViewHolder {
            TextView tv_item_mogu_title, tv_item_mogu_lable, tv_item_mogu_desc, tv_item_mogu_money, tv_item_mogu_surplus;
            ImageView iv_item_mogu_head;

        }

    }


    private void gomogu() {

        /*** 打开蘑菇星球详情页 ** @param context 上下文 * @param taskDataId 子任务id(taskDataId) */
        try {
            MokuHelper.startMokuDetailActivity(context, taskDataId);
        } catch (MokuException e) {
            e.printStackTrace();
        }
    }


    public void getMoGuData(final String type, final int pageIndex) {
        /*** 获取任务列表 ** @param context 上下文 * @param page 页数 * @param pageSize 每页的个数 * @param callBack 网络请求回调接口 */
        mApiDataHelper.getTaskList(context, pageIndex, 100, new com.fendasz.moku.planet.entity.ApiDataCallBack<ClientSampleTaskDataList>() {
            @Override
            public void success(int i, ClientSampleTaskDataList clientSampleTaskDataList) throws Exception {

                List<ClientSampleTaskData> list = clientSampleTaskDataList.getList();
                listsift = new ArrayList<>();

                if (type.equals("全部")) {
                    if (pageIndex == 1) {
                        moguDataListSift = list;
                    } else {
                        for (int i1 = 0; i1 < list.size(); i1++) {
                            moguDataListSift.add(list.get(i1));
                        }
                    }
                } else if (type.equals("截图")) {
                    for (int i1 = 0; i1 < list.size(); i1++) {
                        if (clientSampleTaskDataList.getList().get(i1).getClassify().equals("hp")) {
                            listsift.add(list.get(i1));
                        }
                    }

                    if (pageIndex == 1) {
                        moguDataListSift = listsift;
                    } else {
                        for (int i1 = 0; i1 < listsift.size(); i1++) {
                            moguDataListSift.add(listsift.get(i1));
                        }
                    }

                } else if (type.equals("关键词")) {
                    for (int i1 = 0; i1 < list.size(); i1++) {
                        if (clientSampleTaskDataList.getList().get(i1).getClassify().equals("keyword")) {
                            listsift.add(list.get(i1));
                        }
                    }

                    if (pageIndex == 1) {
                        moguDataListSift = listsift;
                    } else {
                        for (int i1 = 0; i1 < listsift.size(); i1++) {
                            moguDataListSift.add(listsift.get(i1));
                        }
                    }

                } else if (type.equals("评论")) {
                    for (int i1 = 0; i1 < list.size(); i1++) {
                        if (clientSampleTaskDataList.getList().get(i1).getClassify().equals("comment")) {
                            listsift.add(list.get(i1));
                        }
                    }

                    if (pageIndex == 1) {
                        moguDataListSift = listsift;
                    } else {
                        for (int i1 = 0; i1 < listsift.size(); i1++) {
                            moguDataListSift.add(listsift.get(i1));
                        }
                    }

                } else if (type.equals("cpa")) {
                    for (int i1 = 0; i1 < list.size(); i1++) {
                        if (clientSampleTaskDataList.getList().get(i1).getClassify().equals("cpa")) {
                            listsift.add(list.get(i1));
                        }
                    }

                    if (pageIndex == 1) {
                        moguDataListSift = listsift;
                    } else {
                        for (int i1 = 0; i1 < listsift.size(); i1++) {
                            moguDataListSift.add(listsift.get(i1));
                        }
                    }

                }


                hiddenLimitGame();


            }

            @Override
            public void error(int i, String s) throws Exception {
                String sw = s;
            }
        });

    }

    public void hiddenLimitGame() {
        SessionSingleton.getInstance().mysp = getSharedPreferences("limit", context.MODE_PRIVATE);
        String limit = SessionSingleton.getInstance().mysp.getString("limitgame", "") + SessionSingleton.getInstance().limitGame + "";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

            int length = moguDataListSift.size();
            if (!limit.equals("none")) {
                for (int i = 0; i < length; i++) {
                    String adnamecut = moguDataListSift.get(i).getShowName().replace(" ", "");
                    if (limit.indexOf(adnamecut) > -1) {
                        moguDataListSift.remove(i);
                        i = i - 1;
                        length = length - 1;
                    }

                }
            }

            moguDataList = moguDataListSift;
            mLoading.dismiss();
            adapter = new MoGuListAdapter(context);
            xlv_mogu.setAdapter(adapter);

        } else {
            return;
        }


    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mApiDataHelper != null) {
            mApiDataHelper.closeDisposable();
        }
        if (mTTAd != null) {
            mTTAd.destroy();
        }
        if (iad != null) {
            iad.close();
            iad.destroy();
        }

        if (mKsInterstitialAd != null) {
            mKsInterstitialAd = null;
        }
    }


    public void getAd() {

        if (checkedAd == 0) {
            loadExpressAd(SessionSingleton.getInstance().csjcp, 300, 450);
        } else if (checkedAd == 1) {
            iad = getIAD();
            iad.loadAD();
        } else if (checkedAd == 2) {
            requestInterstitialAd();
        } else if (checkedAd == -1) {
            gomogu();
        }
    }

    public void getAdLoadType() {
        try {
            if (mTTAd == null && iad == null && mKsInterstitialAd == null) {
                if (SessionSingleton.getInstance().chuanshanjia.equals("none") &&
                        SessionSingleton.getInstance().guangdiantong.equals("none") && SessionSingleton.getInstance().kuaishou.equals("none")) {
                    checkedAd = -1;
                    return;
                }

                if (!SessionSingleton.getInstance().chuanshanjia.equals("none") && !SessionSingleton.getInstance().guangdiantong.equals("none")
                        && !SessionSingleton.getInstance().kuaishou.equals("none")) {

                    if (checkedAd == 0) {
                        checkedAd = 1;
                    } else if (checkedAd == 1) {
                        checkedAd = 2;
                    } else if (checkedAd == 2) {
                        checkedAd = 0;
                    }

                } else {
                    if (SessionSingleton.getInstance().chuanshanjia.equals("yes") && SessionSingleton.getInstance().guangdiantong.equals("none")
                            && SessionSingleton.getInstance().kuaishou.equals("none")) {
                        checkedAd = 0;
                    } else if (SessionSingleton.getInstance().chuanshanjia.equals("none") && SessionSingleton.getInstance().guangdiantong.equals("yes")
                            && SessionSingleton.getInstance().kuaishou.equals("none")) {
                        checkedAd = 1;
                    } else if (SessionSingleton.getInstance().chuanshanjia.equals("none") && SessionSingleton.getInstance().guangdiantong.equals("none")
                            && SessionSingleton.getInstance().kuaishou.equals("yes")) {
                        checkedAd = 2;
                    } else if (!SessionSingleton.getInstance().chuanshanjia.equals("yes") && SessionSingleton.getInstance().guangdiantong.equals("yes")
                            && !SessionSingleton.getInstance().kuaishou.equals("none")) {
                        checkedAd = 2;
                    } else if (SessionSingleton.getInstance().chuanshanjia.equals("yes") && SessionSingleton.getInstance().guangdiantong.equals("yes")
                            && SessionSingleton.getInstance().kuaishou.equals("none")) {
                        if (checkedAd == 0) {
                            checkedAd = 1;
                        } else {
                            checkedAd = 0;
                        }
                    } else if (SessionSingleton.getInstance().chuanshanjia.equals("yes") && SessionSingleton.getInstance().guangdiantong.equals("none")
                            && SessionSingleton.getInstance().kuaishou.equals("yes")) {
                        if (checkedAd == 0) {
                            checkedAd = 2;
                        } else {
                            checkedAd = 0;
                        }
                    } else if (SessionSingleton.getInstance().chuanshanjia.equals("none") && SessionSingleton.getInstance().guangdiantong.equals("yes")
                            && SessionSingleton.getInstance().kuaishou.equals("yes")) {
                        if (checkedAd == 1) {
                            checkedAd = 2;
                        } else {
                            checkedAd = 1;
                        }
                    }

                }

            }
        } catch (Exception e) {
            String error = e.getMessage();
        }

    }

    //=======================================================快手插屏=============================================================================
    // 1.请求插屏广告，获取广告对象，InterstitialAd
    // 1.请求插屏广告，获取广告对象，InterstitialAd
    public void requestInterstitialAd() {
        if (isAdRequesting) {
            return;
        }
        isAdRequesting = true;
        mKsInterstitialAd = null;
        // 此为测试posId，请联系快手平台申请正式posId
        KsScene scene = new KsScene.Builder(Long.valueOf(SessionSingleton.getInstance().kscp)).build();
        KsAdSDK.getLoadManager().loadInterstitialAd(scene, new KsLoadManager.InterstitialAdListener() {
            @Override
            public void onError(int code, String msg) {
                Toast.makeText(context, "插屏广告请求失败" + code + msg, Toast.LENGTH_SHORT);
                isAdRequesting = false;

                //再次加载穿山甲
                if (SessionSingleton.getInstance().chuanshanjia.equals("none")) {
                    //再次加载广点通
                    if (SessionSingleton.getInstance().guangdiantong.equals("none")) {
                        //加载快手
                        if (SessionSingleton.getInstance().kuaishou.equals("none")) {
                            checkedAd = -1;
                            return;
                        } else {
                            checkedAd = 2;
                        }
                    } else {
                        checkedAd = 1;
                    }
                } else {
                    checkedAd = 0;
                }


                getAd();
            }

            @Override
            public void onRequestResult(int adNumber) {
                //Toast.makeText(context, "插屏广告请求填充个数 " + adNumber, Toast.LENGTH_SHORT);
            }


            @Override
            public void onInterstitialAdLoad(@Nullable List<KsInterstitialAd> adList) {
                isAdRequesting = false;
                if (adList != null && adList.size() > 0) {
                    mKsInterstitialAd = adList.get(0);
                    //Toast.makeText(context, "插屏广告请求成功", Toast.LENGTH_SHORT);

                    KsVideoPlayConfig videoPlayConfig = new KsVideoPlayConfig.Builder()
                            .videoSoundEnable(true)
                            .showLandscape(YunBuMoGuListActivity.this.getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
                            .build();
                    showInterstitialAd(videoPlayConfig);
                }
            }
        });
    }

    private void showInterstitialAd(KsVideoPlayConfig videoPlayConfig) {
        if (mKsInterstitialAd != null) {
            mKsInterstitialAd.setAdInteractionListener(new KsInterstitialAd.AdInteractionListener() {
                @Override
                public void onAdClicked() {
                    // Toast.makeText(context, "插屏广告点击", Toast.LENGTH_SHORT);
                    getAdLoadType();

                }

                @Override
                public void onAdShow() {
                    //Toast.makeText(context, "插屏广告曝光", Toast.LENGTH_SHORT);
                }

                @Override
                public void onAdClosed() {
                    // Toast.makeText(context, "用户点击插屏关闭按钮", Toast.LENGTH_SHORT);
                    mKsInterstitialAd = null;
                    getAdLoadType();
                }

                @Override
                public void onPageDismiss() {
                    Log.i("TestInterstitialAd", "插屏广告关闭");
                    // Toast.makeText(context, "插屏广告关闭", Toast.LENGTH_SHORT);
                    mKsInterstitialAd = null;
                    getAdLoadType();
                    gomogu();
                }

                @Override
                public void onVideoPlayError(int code, int extra) {
                    // Toast.makeText(context, "插屏广告播放出错", Toast.LENGTH_SHORT);
                    mKsInterstitialAd = null;
                    getAdLoadType();
                }

                @Override
                public void onVideoPlayEnd() {
                    //Toast.makeText(context, "插屏广告播放完成", Toast.LENGTH_SHORT);
                    mKsInterstitialAd = null;
                    getAdLoadType();
                }

                @Override
                public void onVideoPlayStart() {
                    //Toast.makeText(context, "插屏广告播放开始", Toast.LENGTH_SHORT);
                }

                @Override
                public void onSkippedAd() {
                    //Toast.makeText(context, "插屏广告播放跳过", Toast.LENGTH_SHORT);
                    mKsInterstitialAd = null;
                    getAdLoadType();
                }

            });
            mKsInterstitialAd.showInterstitialAd(YunBuMoGuListActivity.this, videoPlayConfig);
        } else {
            // Toast.makeText(context, "暂无可用插屏广告，请等待缓存加载或者重新刷新", Toast.LENGTH_SHORT);
        }
    }

    //=======================================================穿山甲插屏=============================================================================
    private void loadExpressAd(String codeId, int expressViewWidth, int expressViewHeight) {
        //step4:创建广告请求参数AdSlot,具体参数含义参考文档

        AdSlot adSlot = new AdSlot.Builder()
                .setCodeId(codeId) //广告位id
                .setAdCount(1) //请求广告数量为1到3条
                .setExpressViewAcceptedSize(expressViewWidth, expressViewHeight) //期望模板广告view的size,单位dp
                .build();
        //step5:请求广告，对请求回调的广告作渲染处理

        mTTAdNative.loadInteractionExpressAd(adSlot, new TTAdNative.NativeExpressAdListener() {
            @Override
            public void onError(int code, String message) {
                // Utils.showToast(context,"load error : " + code + ", " + message);

                //再次加载广点通
                if (SessionSingleton.getInstance().guangdiantong.equals("none")) {
                    //展示失败加载快手
                    if (SessionSingleton.getInstance().kuaishou.equals("none")) {
                        //再次加载穿山甲
                        if (SessionSingleton.getInstance().chuanshanjia.equals("none")) {
                            checkedAd = -1;
                            return;
                        } else {
                            checkedAd = 0;
                        }
                    } else {
                        checkedAd = 2;
                    }
                } else {
                    checkedAd = 1;
                }

                getAd();
            }

            @Override

            public void onNativeExpressAdLoad(List<TTNativeExpressAd> ads) {
                if (ads == null || ads.size() == 0) {
                    return;
                }
                mTTAd = ads.get(0);
                bindAdListener(mTTAd);
                startTime = System.currentTimeMillis();
                mTTAd.render();
                //Utils.showToast(context, "load success !");
            }
        });
    }

    private void bindAdListener(final TTNativeExpressAd ad) {

        ad.setExpressInteractionListener(new TTNativeExpressAd.AdInteractionListener() {
            @Override

            public void onAdDismiss() {
                //Utils.showToast(context, "广告关闭");
                mTTAd = null;
                getAdLoadType();
                gomogu();
            }

            @Override
            public void onAdClicked(View view, int type) {
                //Utils.showToast(context, "广告被点击");
            }

            @Override

            public void onAdShow(View view, int type) {
                // Utils.showToast(context, "广告展示");
            }

            @Override
            public void onRenderFail(View view, String msg, int code) {
                Log.e("ExpressView", "render fail:" + (System.currentTimeMillis() - startTime));
                // Utils.showToast(context, msg + " code:" + code);
            }

            @Override
            public void onRenderSuccess(View view, float width, float height) {
                Log.e("ExpressView", "render suc:" + (System.currentTimeMillis() - startTime));
                //返回view的宽高 单位 dp
                //Utils.showToast(context, "渲染成功");
                ad.showInteractionExpressAd(YunBuMoGuListActivity.this);

            }
        });
        bindDislike(ad, false);

        if (ad.getInteractionType() != TTAdConstant.INTERACTION_TYPE_DOWNLOAD) {
            return;
        }
        ad.setDownloadListener(new TTAppDownloadListener() {
            @Override
            public void onIdle() {
                //Utils.showToast(context, "点击开始下载");
            }

            @Override
            public void onDownloadActive(long totalBytes, long currBytes, String fileName, String appName) {
                if (!mHasShowDownloadActive) {
                    mHasShowDownloadActive = true;
                    // Utils.showToast(context, "下载中，点击暂停");
                }
            }

            @Override
            public void onDownloadPaused(long totalBytes, long currBytes, String fileName, String appName) {
                //Utils.showToast(context, "下载暂停，点击继续");
            }

            @Override
            public void onDownloadFailed(long totalBytes, long currBytes, String fileName, String appName) {
                // Utils.showToast(context, "下载失败，点击重新下载");
            }

            @Override
            public void onInstalled(String fileName, String appName) {
                //Utils.showToast(context, "安装完成，点击图片打开");

            }

            @Override
            public void onDownloadFinished(long totalBytes, String fileName, String appName) {
                //Utils.showToast(context, "点击安装");
            }
        });
    }


    private void bindDislike(TTNativeExpressAd ad, boolean customStyle) {
        if (customStyle) {
            //使用自定义样式
            DislikeInfo dislikeInfo = ad.getDislikeInfo();
            if (dislikeInfo == null || dislikeInfo.getFilterWords() == null || dislikeInfo.getFilterWords().isEmpty()) {
                return;
            }
            final DislikeDialog dislikeDialog = new DislikeDialog(YunBuMoGuListActivity.this, dislikeInfo);
            dislikeDialog.setOnDislikeItemClick(new DislikeDialog.OnDislikeItemClick() {
                @Override
                public void onItemClick(FilterWord filterWord) {
                    //屏蔽广告
                    //Utils.showToast(context,"点击 " + filterWord.getName());
                }
            });
            dislikeDialog.setOnPersonalizationPromptClick(new DislikeDialog.OnPersonalizationPromptClick() {
                @Override
                public void onClick(PersonalizationPrompt personalizationPrompt) {
                    //Utils.showToast(context,"点击了为什么看到此广告");
                }
            });
            ad.setDislikeDialog(dislikeDialog);
            return;
        }
        //使用默认模板中默认dislike弹出样式

        ad.setDislikeCallback(YunBuMoGuListActivity.this, new TTAdDislike.DislikeInteractionCallback() {
            @Override
            public void onShow() {

            }

            @Override
            public void onSelected(int position, String value, boolean enforce) {
                //TToast.show(mContext, "反馈了 " + value);
                Utils.showToast(context, "\t\t\t\t\t\t\t感谢您的反馈!\t\t\t\t\t\t\n我们将为您带来更优质的广告体验");
                if (enforce) {
                    // Utils.showToast(context, "InteractionExpressActivity 模版插屏，穿山甲sdk强制将view关闭了 ");
                }
            }

            @Override
            public void onCancel() {
                //Utils.showToast(context, "点击取消 ");
            }

        });
    }

    //=======================================================广点通插屏=============================================================================
    @Override
    public void onADClicked() {
        getAdLoadType();

    }

    @Override
    public void onADClosed() {
        iad = null;
        getAdLoadType();
        gomogu();
    }

    @Override
    public void onADExposure() {

    }

    @Override
    public void onADLeftApplication() {

    }

    @Override
    public void onADOpened() {

    }

    @Override
    public void onADReceive() {
        if (iad != null) {
            iad.show();
        } else {
            getAdLoadType();
        }
    }

    @Override
    public void onRenderFail() {
        iad = null;
        getAdLoadType();
    }

    @Override
    public void onRenderSuccess() {
        //Log.i("", "onRenderSuccess，建议在此回调后再调用展示方法");
    }

    @Override
    public void onNoAD(AdError adError) {
        iad = null;

        //加载快手
        if (SessionSingleton.getInstance().kuaishou.equals("none")) {
            //展示失败加载穿山甲
            if (SessionSingleton.getInstance().chuanshanjia.equals("none")) {
                //再次加载广点通
                if (SessionSingleton.getInstance().guangdiantong.equals("none")) {
                    checkedAd = -1;
                    return;
                } else {
                    checkedAd = 1;
                }

            } else {
                checkedAd = 0;
            }
        } else {
            checkedAd = 2;
        }

        getAd();
    }

    @Override
    public void onVideoCached() {


    }

    private UnifiedInterstitialAD getIAD() {
        if (this.iad != null) {
            iad.close();
            iad.destroy();
            iad = null;
        }
        iad = new UnifiedInterstitialAD(YunBuMoGuListActivity.this, SessionSingleton.getInstance().gdtcp, this);
        return iad;
    }
}
