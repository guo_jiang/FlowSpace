package com.lt.flowwall.activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fendasz.moku.planet.entity.MokuOptions;
import com.fendasz.moku.planet.utils.PhoneInfoUtils;
import com.lt.flowwall.R;
import com.lt.flowwall.tools.MarqueeTextView;
import com.lt.flowwall.utils.HttpUtils;
import com.lt.flowwall.utils.SessionSingleton;
import com.lt.flowwall.utils.Utils;
import com.lt.flowwall.xPullRefresh.XListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class YunBuIndianaMallActivity extends AppCompatActivity {
    private Context context;
    public Dialog mLoading;
    private PopupWindow rulesPopuo;

    private RelativeLayout rl_indiana_mall_background;
    private ImageView iv_indiana_mall_back;
    private TextView tv_indiana_mall_title, tv_indiana_mall_title_rules;
    private MarqueeTextView mtv_indiana_mall_marquee;
    private TextView tv_yunbu_indiana_title_scoring, tv_yunbu_indiana_mall_title_bills;
    private LinearLayout ll_yunbu_indiana_title_details;
    private XListView xlv_yunbu_indiana_mall_list;

    private IndianaTypeAdapter adapter;
    private JSONArray IndianaTypeArray;

    private String chanelUserAccount, token, MyScore;
    private int isOrNoHint = 0, isOrNoBack = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        if (Build.VERSION.SDK_INT >= 21) {
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        setContentView(R.layout.activity_yun_bu_indiana_mall);

        context = this;

        mLoading = Utils.createLoadingDialog(context, "正在加载......");
        mLoading.setCancelable(false);

        innitview();
        setlistener();
    }

    private void innitview() {
        rl_indiana_mall_background = findViewById(R.id.rl_indiana_mall_background);
        iv_indiana_mall_back = findViewById(R.id.iv_indiana_mall_back);
        tv_indiana_mall_title = findViewById(R.id.tv_indiana_mall_title);
        tv_indiana_mall_title_rules = findViewById(R.id.tv_indiana_mall_title_rules);

        mtv_indiana_mall_marquee = findViewById(R.id.mtv_indiana_mall_marquee);
        tv_yunbu_indiana_title_scoring = findViewById(R.id.tv_yunbu_indiana_title_scoring);
        ll_yunbu_indiana_title_details = findViewById(R.id.ll_yunbu_indiana_title_details);
        tv_yunbu_indiana_mall_title_bills = findViewById(R.id.tv_yunbu_indiana_mall_title_bills);

        xlv_yunbu_indiana_mall_list = findViewById(R.id.xlv_yunbu_indiana_mall_list);


        IndianaTypeArray = new JSONArray();
        try {
            token = SessionSingleton.getInstance().AccountSingle.getString("token");
            chanelUserAccount = SessionSingleton.getInstance().AccountSingle.getString("chanelUserAccount");
            MyScore = SessionSingleton.getInstance().WelfareSingle.getString("score");

            if (SessionSingleton.getInstance().hasStyleConfig == 1) {
                tv_indiana_mall_title.setTextColor(SessionSingleton.getInstance().mYBStyleConfig.getTitleTextColor());
                tv_indiana_mall_title_rules.setTextColor(SessionSingleton.getInstance().mYBStyleConfig.getTitleTextColor());
                rl_indiana_mall_background.setBackgroundColor(SessionSingleton.getInstance().mYBStyleConfig.getTitleBackColor());


                if (SessionSingleton.getInstance().mYBStyleConfig.getTitleBackIcon() == 0) {
                    iv_indiana_mall_back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_back_black));
                } else {
                    iv_indiana_mall_back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_back_write));
                }
            }
            tv_yunbu_indiana_title_scoring.setText(MyScore);


            JSONArray lunboVoice = SessionSingleton.getInstance().AccountSingle.getJSONArray("duobaoZJPaomadengArray");
            for (int i = 0; i < lunboVoice.length(); i++) {
                SessionSingleton.getInstance().marqueeArray.put("用户ID:" + lunboVoice.getJSONObject(i).getString("channelUserId") + "参与夺宝中奖," + lunboVoice.
                        getJSONObject(i).getString("productName"));
            }

            MallData();

            mtv_indiana_mall_marquee.setTipList(null);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        xlv_yunbu_indiana_mall_list.setPullRefreshEnable(true);
        xlv_yunbu_indiana_mall_list.setPullLoadEnable(false);
        adapter = new IndianaTypeAdapter(context);
        xlv_yunbu_indiana_mall_list.setXListViewListener(new XListView.IXListViewListener() {

            @Override
            public void onRefresh() {
                mLoading.show();
                MallData();

                xlv_yunbu_indiana_mall_list.stopRefresh();
            }

            @Override
            public void onLoadMore() {
                xlv_yunbu_indiana_mall_list.stopLoadMore();
            }

        });

        xlv_yunbu_indiana_mall_list.setAdapter(adapter);
    }


    private void setlistener() {
        iv_indiana_mall_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //规则
        tv_indiana_mall_title_rules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRules();
            }
        });

        //获取积分
        ll_yunbu_indiana_title_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(YunBuIndianaMallActivity.this, YunBuNavigateActivity.class);
                SessionSingleton.getInstance().backFragemntNum = 2;
                startActivity(intent);
            }
        });

        //夺宝记录
        tv_yunbu_indiana_mall_title_bills.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(YunBuIndianaMallActivity.this, YunBuIndianaBillActivity.class);
                intent.putExtra("INDIANABILLTYPE", "duobao");
                startActivity(intent);
            }
        });


        /*xlv_yunbu_indiana_mall_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    JSONObject object = IndianaTypeArray.getJSONObject(position);
                    SessionSingleton.getInstance().IndianaDetailsSingle = object;

                    Intent intent = new Intent(YunBuIndianaMallActivity.this, YunBuIndianaDetailsActivity.class);
                    if (object.getString("type").equals("红包")) {
                        intent.putExtra("INDIANA_TASK_TYPE", "hongbao");
                    } else if (object.getString("type").equals("活跃度")) {
                        intent.putExtra("INDIANA_TASK_TYPE", "huoyuedu");
                    }
                    startActivity(intent);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });*/
    }

    public class IndianaTypeAdapter extends BaseAdapter {

        private LayoutInflater inflater;


        public IndianaTypeAdapter(Context context) {
            inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            if (IndianaTypeArray.length() == 0) {
                return 0;
            } else {
                int num = (IndianaTypeArray.length() % 2);
                int count = IndianaTypeArray.length() / 2;
                if (num != 0) {
                    return (count + 1);
                }
                return (count);
            }
        }

        @Override
        public Object getItem(int position) {

            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;

            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_yun_bu_indiana_item, null);
            holder.ll_indiana_item_one = (LinearLayout) convertView.findViewById(R.id.ll_indiana_item_one);
            holder.iv_indiana_item_image_type_one = (ImageView) convertView.findViewById(R.id.iv_indiana_item_image_type_one);
            holder.pb_indiana_item_progress_one = (ProgressBar) convertView.findViewById(R.id.pb_indiana_item_progress_one);
            holder.iv_indiana_item_title_one = (TextView) convertView.findViewById(R.id.iv_indiana_item_title_one);
            holder.iv_indiana_item_price_one = (TextView) convertView.findViewById(R.id.iv_indiana_item_price_one);
            holder.iv_indiana_item_join_one = (TextView) convertView.findViewById(R.id.iv_indiana_item_join_one);

            holder.ll_indiana_item_two = (LinearLayout) convertView.findViewById(R.id.ll_indiana_item_two);
            holder.iv_indiana_item_image_type_two = (ImageView) convertView.findViewById(R.id.iv_indiana_item_image_type_two);
            holder.pb_indiana_item_progress_two = (ProgressBar) convertView.findViewById(R.id.pb_indiana_item_progress_two);
            holder.iv_indiana_item_title_two = (TextView) convertView.findViewById(R.id.iv_indiana_item_title_two);
            holder.iv_indiana_item_price_two = (TextView) convertView.findViewById(R.id.iv_indiana_item_price_two);
            holder.iv_indiana_item_join_two = (TextView) convertView.findViewById(R.id.iv_indiana_item_join_two);

            try {
                //JSONObject single = homeArray.getJSONObject(position - 1);
                JSONObject single = null;
                JSONObject singlea = null;
                if (position == 0) {
                    single = IndianaTypeArray.getJSONObject(0);

                    if (IndianaTypeArray.length() < 2) {
                        holder.ll_indiana_item_two.setVisibility(View.GONE);
                        isOrNoHint = 1;
                    } else {
                        isOrNoHint = 0;
                        singlea = IndianaTypeArray.getJSONObject(1);
                    }
                } else {
                    single = IndianaTypeArray.getJSONObject(2 * position);
                    if (IndianaTypeArray.length() - 1 < (2 * position + 1)) {
                        holder.ll_indiana_item_two.setVisibility(View.GONE);
                        isOrNoHint = 1;
                    } else {
                        isOrNoHint = 0;
                        singlea = IndianaTypeArray.getJSONObject(2 * position + 1);

                    }


                }

                if (single.getString("type").equals("红包")) {
                    holder.iv_indiana_item_image_type_one.setImageResource(R.mipmap.ic_yun_bu_indiana_redpacket);
                } else if (single.getString("type").contains("活跃")) {
                    holder.iv_indiana_item_image_type_one.setImageResource(R.mipmap.ic_yun_bu_indiana_details_integral);
                }

                holder.pb_indiana_item_progress_one.setProgress(single.getInt("currentTimes") * single.getInt("needTimes") / 100);
                holder.iv_indiana_item_title_one.setText(single.getString("goodsName"));
                holder.iv_indiana_item_price_one.setText(single.getString("consumeScore") + "活跃值/次");

                final JSONObject finalSingle = single;
                holder.ll_indiana_item_one.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            SessionSingleton.getInstance().IndianaDetailsSingle = finalSingle;
                            Intent intent = new Intent(YunBuIndianaMallActivity.this, YunBuIndianaDetailsActivity.class);
                            if (finalSingle.getString("type").equals("红包")) {
                                intent.putExtra("INDIANA_TASK_TYPE", "hongbao");
                            } else if (finalSingle.getString("type").contains("活跃")) {
                                intent.putExtra("INDIANA_TASK_TYPE", "huoyuedu");
                            }
                            startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });


                if (isOrNoHint == 0) {
                    if (singlea.getString("type").equals("红包")) {
                        holder.iv_indiana_item_image_type_two.setImageResource(R.mipmap.ic_yun_bu_indiana_redpacket);
                    } else if (singlea.getString("type").contains("活跃")) {
                        holder.iv_indiana_item_image_type_two.setImageResource(R.mipmap.ic_yun_bu_indiana_details_integral);
                    }

                    holder.pb_indiana_item_progress_two.setProgress(singlea.getInt("currentTimes") * singlea.getInt("needTimes") / 100);

                    holder.iv_indiana_item_title_two.setText(singlea.getString("goodsName"));
                    holder.iv_indiana_item_price_two.setText(singlea.getString("consumeScore") + "活跃值/次");

                    final JSONObject finalSinglea = singlea;
                    holder.ll_indiana_item_two.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                SessionSingleton.getInstance().IndianaDetailsSingle = finalSinglea;
                                Intent intent = new Intent(YunBuIndianaMallActivity.this, YunBuIndianaDetailsActivity.class);
                                if (finalSinglea.getString("type").equals("红包")) {
                                    intent.putExtra("INDIANA_TASK_TYPE", "hongbao");
                                } else if (finalSinglea.getString("type").contains("活跃")) {
                                    intent.putExtra("INDIANA_TASK_TYPE", "huoyuedu");
                                }
                                startActivity(intent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }


            } catch (Exception e) {
                e.printStackTrace();
            }


            return convertView;
        }


        class ViewHolder {
            LinearLayout ll_indiana_item_one;
            ImageView iv_indiana_item_image_type_one;
            ProgressBar pb_indiana_item_progress_one;
            TextView iv_indiana_item_title_one, iv_indiana_item_price_one, iv_indiana_item_join_one;

            LinearLayout ll_indiana_item_two;
            ImageView iv_indiana_item_image_type_two;
            ProgressBar pb_indiana_item_progress_two;
            TextView iv_indiana_item_title_two, iv_indiana_item_price_two, iv_indiana_item_join_two;
        }

    }

    //刷新页面
    private void refreshData() {
        Map<String, String> params = new HashMap<>();
        params.put("chanelUserAccount", chanelUserAccount);
        params.put("token", token);
        HttpUtils.doHttpReqeust("POST", SessionSingleton.getInstance().requestBaseUrl + "everyDayTaskfreshApi?", params, new HttpUtils.StringCallback() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject returnJSONObject = new JSONObject(response);
                    if (returnJSONObject.getString("status").equals("success")) {

                        SessionSingleton.getInstance().WelfareSingle.put("score", returnJSONObject.getString("score"));
                        MyScore = SessionSingleton.getInstance().WelfareSingle.getString("score");
                        tv_yunbu_indiana_title_scoring.setText(MyScore);
                        adapter.notifyDataSetChanged();
                    } else {
                        Utils.showToast(context, returnJSONObject.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mLoading.dismiss();
            }

            @Override
            public void onFaileure(int code, Exception e) {
                mLoading.dismiss();
                e.printStackTrace();
            }
        });
    }

    //获取商品
    private void MallData() {
        Map<String, String> params = new HashMap<>();
        params.put("chanelUserAccount", chanelUserAccount);
        params.put("token", token);
        HttpUtils.doHttpReqeust("POST", SessionSingleton.getInstance().requestBaseUrl + "getDuobaoGoodsApi?", params, new HttpUtils.StringCallback() {

            @Override
            public void onSuccess(String response) {

                try {
                    JSONObject returnJSONObject = new JSONObject(response);
                    if (returnJSONObject.getString("status").equals("success")) {
                        IndianaTypeArray = returnJSONObject.getJSONArray("data");
                        isOrNoBack = 1;
                        refreshData();

                        adapter.notifyDataSetChanged();
                    } else {
                        Utils.showToast(context, returnJSONObject.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mLoading.dismiss();
            }

            @Override
            public void onFaileure(int code, Exception e) {
                mLoading.dismiss();
                e.printStackTrace();
            }
        });
    }

    //提示
    private void showRules() {

        LayoutInflater la = LayoutInflater.from(context);
        View contentView = la.inflate(R.layout.pop_yun_bu_indiana_rules_show, null);//自定义布局
        ImageView iv_pop_indiana_rules_close = contentView.findViewById(R.id.iv_pop_indiana_rules_close);

        iv_pop_indiana_rules_close.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                rulesPopuo.dismiss();
            }
        });


        rulesPopuo = new PopupWindow(contentView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);


        //设置PopupWindow的焦点
        rulesPopuo.setFocusable(true);
        rulesPopuo.setClippingEnabled(false);
        //点击PopupWindow之外的地方PopupWindow会消失
        rulesPopuo.setOutsideTouchable(true);
        //showAtLocation(View parent, int gravity, int x, int y)：相对于父控件的位置（例如正中央Gravity.CENTER，下方Gravity.BOTTOM等），可以设置偏移或无偏移
        rulesPopuo.showAtLocation(this.getWindow().getDecorView(), Gravity.CENTER, 0, 0);
        rulesPopuo.update();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isOrNoBack != 0) {
            mLoading.show();
            MallData();
        }
    }
}
