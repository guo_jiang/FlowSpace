package com.lt.flowwall.activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lt.flowwall.R;
import com.lt.flowwall.utils.HttpUtils;
import com.lt.flowwall.utils.SessionSingleton;
import com.lt.flowwall.utils.Utils;
import com.lt.flowwall.xPullRefresh.XListView;
import com.lt.flowwall.yunbuimageload.AsyncImageLoader;
import com.lt.flowwall.yunbuimageload.FileCache;
import com.lt.flowwall.yunbuimageload.MemoryCache;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class YunBuIndianaBillActivity extends AppCompatActivity {
    private Context context;
    public Dialog mLoading;

    private JSONArray indianaBillArray;
    private IndianaBillAdapter adapter;

    private XListView xlv_indiana_bill;
    private ImageView iv_indiana_bill_back;
    private TextView tv_indiana_bill_title;
    private RelativeLayout rl_indiana_bill_background;

    private TextView tv_indiana_bill_indiana, tv_indiana_bill_luckpan;

    private String indianaType = "duobao";
    private int page;

    private String chanelUserAccount, token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        if (Build.VERSION.SDK_INT >= 21) {
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        setContentView(R.layout.activity_indiana_bill);

        context = this;

        Intent intent = getIntent();
        indianaType = intent.getStringExtra("INDIANABILLTYPE");


        mLoading = Utils.createLoadingDialog(context, "正在加载......");
        mLoading.setCancelable(false);

        innitview();
        setListener();
    }

    private void innitview() {
        xlv_indiana_bill = findViewById(R.id.xlv_indiana_bill);
        iv_indiana_bill_back = findViewById(R.id.iv_indiana_bill_back);
        tv_indiana_bill_title = findViewById(R.id.tv_indiana_bill_title);
        rl_indiana_bill_background = findViewById(R.id.rl_indiana_bill_background);

        tv_indiana_bill_indiana = findViewById(R.id.tv_indiana_bill_indiana);
        tv_indiana_bill_luckpan = findViewById(R.id.tv_indiana_bill_luckpan);

        indianaBillArray = new JSONArray();

        page = 1;

        try {
            token = SessionSingleton.getInstance().AccountSingle.getString("token");
            chanelUserAccount = SessionSingleton.getInstance().AccountSingle.getString("chanelUserAccount");

            if (SessionSingleton.getInstance().hasStyleConfig == 1) {
                tv_indiana_bill_title.setTextColor(SessionSingleton.getInstance().mYBStyleConfig.getTitleTextColor());
                rl_indiana_bill_background.setBackgroundColor(SessionSingleton.getInstance().mYBStyleConfig.getTitleBackColor());

                if (SessionSingleton.getInstance().mYBStyleConfig.getTitleBackIcon() == 0) {
                    iv_indiana_bill_back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_back_black));
                } else {
                    iv_indiana_bill_back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_back_write));
                }
            }

            if (indianaType.equals("duobao")) {
                tv_indiana_bill_indiana.setBackground(getResources().getDrawable(R.color.yunbu_lightgreen));
                tv_indiana_bill_luckpan.setBackground(getResources().getDrawable(R.color.yunbu_write));

                tv_indiana_bill_indiana.setTextColor(getResources().getColor(R.color.yunbu_write));
                tv_indiana_bill_luckpan.setTextColor(getResources().getColor(R.color.yunbu_textblack));
            } else {
                tv_indiana_bill_indiana.setBackground(getResources().getDrawable(R.color.yunbu_write));
                tv_indiana_bill_luckpan.setBackground(getResources().getDrawable(R.color.yunbu_lightgreen));

                tv_indiana_bill_indiana.setTextColor(getResources().getColor(R.color.yunbu_textblack));
                tv_indiana_bill_luckpan.setTextColor(getResources().getColor(R.color.yunbu_write));
            }

            mLoading.show();
            page = 1;
            DetailsListData(indianaType, page);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        xlv_indiana_bill.setPullRefreshEnable(true);
        xlv_indiana_bill.setPullLoadEnable(true);
        xlv_indiana_bill.setXListViewListener(new XListView.IXListViewListener() {

            @Override
            public void onRefresh() {
                page = 1;
                mLoading.show();
                DetailsListData(indianaType, page);
                Load();
            }

            @Override
            public void onLoadMore() {
                page = page + 1;
                mLoading.show();
                DetailsListData(indianaType, page);
                Load();
            }

        });
        adapter = new IndianaBillAdapter(context);
        xlv_indiana_bill.setAdapter(adapter);

    }

    private void Load() {
        xlv_indiana_bill.stopLoadMore();
        xlv_indiana_bill.stopRefresh();
    }


    private void setListener() {
        iv_indiana_bill_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tv_indiana_bill_indiana.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_indiana_bill_indiana.setBackground(getResources().getDrawable(R.color.yunbu_lightgreen));
                tv_indiana_bill_luckpan.setBackground(getResources().getDrawable(R.color.yunbu_write));

                tv_indiana_bill_indiana.setTextColor(getResources().getColor(R.color.yunbu_write));
                tv_indiana_bill_luckpan.setTextColor(getResources().getColor(R.color.yunbu_textblack));

                mLoading.show();
                indianaType = "duobao";
                page = 1;
                DetailsListData(indianaType, page);
            }
        });

        tv_indiana_bill_luckpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_indiana_bill_indiana.setBackground(getResources().getDrawable(R.color.yunbu_write));
                tv_indiana_bill_luckpan.setBackground(getResources().getDrawable(R.color.yunbu_lightgreen));

                tv_indiana_bill_indiana.setTextColor(getResources().getColor(R.color.yunbu_textblack));
                tv_indiana_bill_luckpan.setTextColor(getResources().getColor(R.color.yunbu_write));

                mLoading.show();
                indianaType = "choujiang";
                page = 1;
                DetailsListData(indianaType, page);
            }
        });
    }


    public class IndianaBillAdapter extends BaseAdapter {
        private AsyncImageLoader imageLoader;//异步组件
        private LayoutInflater inflater;


        public IndianaBillAdapter(Context context) {

            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            MemoryCache mcache = new MemoryCache();//内存缓存
            String paht = getApplicationContext().getFilesDir().getAbsolutePath();
            File cacheDir = new File(paht, "yunbucache");//缓存根目录
            FileCache fcache = new FileCache(context, cacheDir, "yunbuimage");//文件缓存
            imageLoader = new AsyncImageLoader(context, mcache, fcache);
        }

        @Override
        public int getCount() {
            return indianaBillArray.length();
        }

        @Override
        public Object getItem(int position) {

            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            ViewIndianaHolder indianaHolder;

            if (indianaType.equals("choujiang")) {
                // if (convertView == null) {
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.item_yun_bu_indiana_details_item, null);
                holder.ll_indiana_details_item_bg = (LinearLayout) convertView.findViewById(R.id.ll_indiana_details_item_bg);
                holder.iv_indiana_details_item_head = (ImageView) convertView.findViewById(R.id.iv_indiana_details_item_head);
                holder.tv_indiana_details_item_title = (TextView) convertView.findViewById(R.id.tv_indiana_details_item_title);
                holder.tv_indiana_details_item_date = (TextView) convertView.findViewById(R.id.tv_indiana_details_item_date);
                holder.tv_indiana_details_item_times = (TextView) convertView.findViewById(R.id.tv_indiana_details_item_times);
                holder.tv_indiana_details_join_num = (TextView) convertView.findViewById(R.id.tv_indiana_details_join_num);
                convertView.setTag(holder);
               /* } else {
                    holder = (ViewHolder) convertView.getTag();
                }*/

                try {
                    JSONObject single = indianaBillArray.getJSONObject(position);

                    holder.ll_indiana_details_item_bg.setVisibility(View.GONE);
                    holder.tv_indiana_details_join_num.setVisibility(View.GONE);

                    holder.tv_indiana_details_item_title.setText(single.getString("title"));
                    holder.tv_indiana_details_item_date.setText(single.getString("datetime"));
                    holder.tv_indiana_details_item_times.setText(single.getString("content"));


                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                // if (convertView == null) {
                indianaHolder = new ViewIndianaHolder();
                convertView = inflater.inflate(R.layout.item_yun_bu_indiana_bill_item, null);
                indianaHolder.iv_indiana_bill_item_head = (ImageView) convertView.findViewById(R.id.iv_indiana_bill_item_head);
                indianaHolder.tv_indiana_bill_item_title = (TextView) convertView.findViewById(R.id.tv_indiana_bill_item_title);
                indianaHolder.tv_indiana_bill_item_stutas = (TextView) convertView.findViewById(R.id.tv_indiana_bill_item_stutas);
                indianaHolder.tv_indiana_bill_item_number = (TextView) convertView.findViewById(R.id.tv_indiana_bill_item_number);
                indianaHolder.tv_indiana_bill_item_join_number = (TextView) convertView.findViewById(R.id.tv_indiana_bill_item_join_number);
                indianaHolder.tv_indiana_bill_item_date = (TextView) convertView.findViewById(R.id.tv_indiana_bill_item_date);
                indianaHolder.tv_indiana_bill_item_times = (TextView) convertView.findViewById(R.id.tv_indiana_bill_item_times);
                convertView.setTag(indianaHolder);
               /* } else {
                    indianaHolder = (ViewIndianaHolder) convertView.getTag();
                }*/

                try {
                    JSONObject single = indianaBillArray.getJSONObject(position);

                    if (single.getString("type").contains("活跃")) {
                        indianaHolder.iv_indiana_bill_item_head.setBackground(getResources().getDrawable(R.mipmap.ic_yun_bu_indiana_details_integral));
                    } else {
                        indianaHolder.iv_indiana_bill_item_head.setBackground(getResources().getDrawable(R.mipmap.ic_yun_bu_indiana_details_redpacket));
                    }

                    indianaHolder.tv_indiana_bill_item_title.setText(single.getString("productName"));
                    indianaHolder.tv_indiana_bill_item_number.setText("期号：" + single.getString("dateNumber"));
                    indianaHolder.tv_indiana_bill_item_date.setText(single.getString("duobaoData"));
                    indianaHolder.tv_indiana_bill_item_times.setText("参与" + single.getString("joinTimes") + "次");
                    if (single.getString("beginNumber").equals(single.getString("endNumber"))) {
                        indianaHolder.tv_indiana_bill_item_join_number.setText("参与号：" + single.getString("beginNumber"));
                    } else {
                        indianaHolder.tv_indiana_bill_item_join_number.setText("参与号：" + single.getString("beginNumber") + "  -  " + single.getString("endNumber"));
                    }

                    if (single.getString("rewardStatus").equals("yes")) {
                        String money = single.getString("productName").replace("奖励", "");
                        indianaHolder.tv_indiana_bill_item_stutas.setText("+" + money);
                    } else if (single.getString("rewardStatus").equals("none")) {
                        indianaHolder.tv_indiana_bill_item_stutas.setText("待开奖");
                    }else{
                        indianaHolder.tv_indiana_bill_item_stutas.setText("未中奖");
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            return convertView;
        }

        class ViewIndianaHolder {
            TextView tv_indiana_bill_item_title, tv_indiana_bill_item_stutas, tv_indiana_bill_item_number, tv_indiana_bill_item_date, tv_indiana_bill_item_times;
            TextView tv_indiana_bill_item_join_number;
            ImageView iv_indiana_bill_item_head;
        }

        class ViewHolder {
            TextView tv_indiana_details_item_title, tv_indiana_details_item_date, tv_indiana_details_item_times, tv_indiana_details_join_num;
            ImageView iv_indiana_details_item_head;
            LinearLayout ll_indiana_details_item_bg;
        }

    }


    //获取列表
    private void DetailsListData(String type, final int pageindex) {
        Map<String, String> params = new HashMap<>();
        params.put("chanelUserAccount", chanelUserAccount);
        params.put("token", token);
        params.put("page", String.valueOf(pageindex));
        params.put("type", type);
        HttpUtils.doHttpReqeust("POST", SessionSingleton.getInstance().requestBaseUrl + "duobaoAndChouJiangJoinRecordApi?", params, new HttpUtils.StringCallback() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject returnJSONObject = new JSONObject(response);
                    if (returnJSONObject.getString("status").equals("success")) {

                        JSONArray array = returnJSONObject.getJSONArray("data");
                        if (pageindex == 1) {
                            indianaBillArray = array;
                        } else {
                            for (int i = 0; i < array.length(); i++) {
                                indianaBillArray.put(array.getJSONObject(i));
                            }
                        }
                        adapter.notifyDataSetChanged();
                    } else {
                        Utils.showToast(context, returnJSONObject.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mLoading.dismiss();
            }

            @Override
            public void onFaileure(int code, Exception e) {
                mLoading.dismiss();
                e.printStackTrace();
            }
        });
    }
}
