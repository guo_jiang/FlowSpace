package com.lt.flowwall.activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bytedance.sdk.openadsdk.TTAdConstant;
import com.lt.flowwall.R;
import com.lt.flowwall.utils.SessionSingleton;
import com.lt.flowwall.xPullRefresh.XListView;
import com.lt.flowwall.yunbuimageload.AsyncImageLoader;
import com.lt.flowwall.yunbuimageload.FileCache;
import com.lt.flowwall.yunbuimageload.MemoryCache;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class YunBuTakeOutActivity extends AppCompatActivity {
    private Context context;

    private GridView gv_welfare_take_out_list;
    private ImageView iv_welfare_take_out_back;
    private TextView tv_welfare_take_out_title;
    private RelativeLayout rl_welfare_take_out_background;

    private TakeOutAdapter adapter;
    private JSONArray recommendcoupons;

    private PopupWindow toastPopuo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        if (Build.VERSION.SDK_INT >= 21) {
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        setContentView(R.layout.activity_yun_bu_take_out);

        context = this;

        initview();
        setlistener();
    }

    private void initview() {
        gv_welfare_take_out_list = findViewById(R.id.gv_welfare_take_out_list);
        iv_welfare_take_out_back = findViewById(R.id.iv_welfare_take_out_back);
        tv_welfare_take_out_title = findViewById(R.id.tv_welfare_take_out_title);
        rl_welfare_take_out_background = findViewById(R.id.rl_welfare_take_out_background);

        try {
            recommendcoupons = SessionSingleton.getInstance().AccountSingle.getJSONArray("AAAAAfuliArray");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (SessionSingleton.getInstance().hasStyleConfig == 1) {
            tv_welfare_take_out_title.setTextColor(SessionSingleton.getInstance().mYBStyleConfig.getTitleTextColor());
            rl_welfare_take_out_background.setBackgroundColor(SessionSingleton.getInstance().mYBStyleConfig.getTitleBackColor());

            if (SessionSingleton.getInstance().mYBStyleConfig.getTitleBackIcon() == 0) {
                iv_welfare_take_out_back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_back_black));
            } else {
                iv_welfare_take_out_back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_back_write));
            }
        }

        adapter = new TakeOutAdapter(context);
        gv_welfare_take_out_list.setAdapter(adapter);
    }

    private void setlistener() {
        iv_welfare_take_out_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    public class TakeOutAdapter extends BaseAdapter {
        private AsyncImageLoader imageLoader;//异步组件
        private LayoutInflater inflater;


        public TakeOutAdapter(Context context) {

            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            MemoryCache mcache = new MemoryCache();//内存缓存
            String paht = getApplicationContext().getFilesDir().getAbsolutePath();
            File cacheDir = new File(paht, "yunbucache");//缓存根目录
            FileCache fcache = new FileCache(context, cacheDir, "yunbuimage");//文件缓存
            imageLoader = new AsyncImageLoader(context, mcache, fcache);
        }

        @Override
        public int getCount() {
            return recommendcoupons.length();
        }

        @Override
        public Object getItem(int position) {

            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;

            if (convertView == null) {
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.item_yun_bu_take_out_item, null);
                holder.ll_take_out_item = (LinearLayout) convertView.findViewById(R.id.ll_take_out_item);
                holder.iv_take_out_item_head = (ImageView) convertView.findViewById(R.id.iv_take_out_item_head);
                holder.tv_take_out_item_title = (TextView) convertView.findViewById(R.id.tv_take_out_item_title);
                holder.tv_take_out_item_type = (TextView) convertView.findViewById(R.id.tv_take_out_item_type);
                holder.tv_take_out_item_money = (TextView) convertView.findViewById(R.id.tv_take_out_item_money);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            try {
                final JSONObject single = recommendcoupons.getJSONObject(position);

                //异步加载图片，先从一级缓存、再二级缓存、最后网络获取图片
                Bitmap bmp1 = imageLoader.loadBitmap(holder.iv_take_out_item_head, single.getString("image"));
                if (bmp1 == null) {
                    holder.iv_take_out_item_head.setImageResource(R.drawable.ic_load_iname);
                } else {
                    holder.iv_take_out_item_head.setImageBitmap(bmp1);
                }

                if (single.getString("title").contains("活跃")) {
                    holder.tv_take_out_item_type.setText("活跃值");
                    holder.tv_take_out_item_money.setText("+" + Math.round(single.getInt("redPacket")));
                } else if (single.getString("title").contains("红包")) {
                    holder.tv_take_out_item_type.setText("红包");
                    holder.tv_take_out_item_money.setText("+" + single.getString("redPacket"));
                }
                holder.tv_take_out_item_title.setText(single.getString("title"));


                holder.ll_take_out_item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            showToast(single.getString("description"), single.getString("jumpUrl"), single.getString("package"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }


            return convertView;
        }

        class ViewHolder {
            TextView tv_take_out_item_title,tv_take_out_item_type, tv_take_out_item_money;
            ImageView iv_take_out_item_head;
            LinearLayout ll_take_out_item;
        }

    }

    //提示
    private void showToast(final String msg, final String jumpUrl, final String packagename) {

        LayoutInflater la = LayoutInflater.from(context);
        View contentView = la.inflate(R.layout.pop_yun_bu_toast_show, null);//自定义布局
        TextView tv_toast_show__msg = contentView.findViewById(R.id.tv_toast_show__msg);
        TextView tv_toast_show_close = contentView.findViewById(R.id.tv_toast_show_close);
        ImageView iv_toast_show_close = contentView.findViewById(R.id.iv_toast_show_close);

        tv_toast_show__msg.setText(msg);

        tv_toast_show_close.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                toastPopuo.dismiss();
                openApp(jumpUrl, packagename);

            }
        });
        iv_toast_show_close.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                toastPopuo.dismiss();
            }
        });

        toastPopuo = new PopupWindow(contentView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);


        //设置PopupWindow的焦点
        toastPopuo.setFocusable(true);
        toastPopuo.setClippingEnabled(false);
        //点击PopupWindow之外的地方PopupWindow会消失
        toastPopuo.setOutsideTouchable(true);
        //showAtLocation(View parent, int gravity, int x, int y)：相对于父控件的位置（例如正中央Gravity.CENTER，下方Gravity.BOTTOM等），可以设置偏移或无偏移
        toastPopuo.showAtLocation(YunBuTakeOutActivity.this.getWindow().getDecorView(), Gravity.CENTER, 0, 0);
        toastPopuo.update();
    }


    /**
     * DPlink拉起App
     */
    public void openApp(String url, String packageName) {
        if (checkPackage(packageName)) {
            String content = url;
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(content));
            startActivity(intent);
        }
    }


    /**
     * 检测该包名所对应的应用是否存在
     * * @param packageName
     *
     * @return
     */
    public boolean checkPackage(String packageName) {
        if (packageName == null || "".equals(packageName)) return false;
        try {
            //手机已安装，返回true
            getPackageManager().getApplicationInfo(packageName, PackageManager.GET_UNINSTALLED_PACKAGES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            //手机未安装，跳转到应用商店下载，并返回false
            Uri uri = Uri.parse("market://details?id=" + packageName);
            Intent it = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(it);
            return false;
        }
    }
}
