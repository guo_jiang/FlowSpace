package com.lt.flowwall.activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lt.flowwall.R;
import com.lt.flowwall.fragment.YunBuWelfareFragment;
import com.lt.flowwall.utils.HttpUtils;
import com.lt.flowwall.utils.SessionSingleton;
import com.lt.flowwall.utils.Utils;
import com.lt.flowwall.xPullRefresh.XListView;
import com.lt.flowwall.yunbuimageload.AsyncImageLoader;
import com.lt.flowwall.yunbuimageload.FileCache;
import com.lt.flowwall.yunbuimageload.MemoryCache;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class YunBuIndianaDetailsActivity extends AppCompatActivity {
    private Context context;
    public Dialog mLoading;
    private PopupWindow timesPopuo;

    private XListView xlv_indiana_details;
    private ImageView iv_indiana_details_back;
    private TextView tv_indiana_details_title, tv_indiana_details_title_bill;
    private RelativeLayout rl_indiana_details_background;

    private TextView tv_indiana_details_bottom_start;
    private LinearLayout ll_indiana_details_bottom;

    private IndianaDetailsAdapter adapter;
    private JSONArray detailsListArray;
    private JSONObject detailsSingle;

    private String indianaType;
    private int participateTimes = 1, surplusTimes = 0, price = 0, surplusIntegral = 0, joinStatus = 0, ISORNOFIRST = 0;

    private int page;
    private String chanelUserAccount, token, productId, dateNumber, channelUserId;

    private CountTimer countTimer;

    private AlertDialog.Builder builder;
    private AlertDialog dialog;
    private LayoutInflater inflater;
    private View layout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        if (Build.VERSION.SDK_INT >= 21) {
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        setContentView(R.layout.activity_indiana_details);

        context = this;

        Intent intent = getIntent();

        indianaType = intent.getStringExtra("INDIANA_TASK_TYPE");

        mLoading = Utils.createLoadingDialog(context, "正在加载......");
        mLoading.setCancelable(false);

        innitview();
        setlistener();
    }

    private void innitview() {
        xlv_indiana_details = findViewById(R.id.xlv_indiana_details);
        iv_indiana_details_back = findViewById(R.id.iv_indiana_details_back);
        tv_indiana_details_title = findViewById(R.id.tv_indiana_details_title);
        rl_indiana_details_background = findViewById(R.id.rl_indiana_details_background);
        tv_indiana_details_title_bill = findViewById(R.id.tv_indiana_details_title_bill);

        ll_indiana_details_bottom = findViewById(R.id.ll_indiana_details_bottom);
        tv_indiana_details_bottom_start = findViewById(R.id.tv_indiana_details_bottom_start);

        detailsListArray = new JSONArray();

        try {
            token = SessionSingleton.getInstance().AccountSingle.getString("token");
            chanelUserAccount = SessionSingleton.getInstance().AccountSingle.getString("chanelUserAccount");
            channelUserId = SessionSingleton.getInstance().AccountSingle.getString("id");

            detailsSingle = SessionSingleton.getInstance().IndianaDetailsSingle;

            dateNumber = detailsSingle.getString("dateNumber");
            productId = detailsSingle.getString("id");

            surplusIntegral = SessionSingleton.getInstance().WelfareSingle.getInt("score");
            price = detailsSingle.getInt("consumeScore");
            surplusTimes = detailsSingle.getInt("needTimes") - detailsSingle.getInt("currentTimes");

            if (SessionSingleton.getInstance().hasStyleConfig == 1) {
                tv_indiana_details_title.setTextColor(SessionSingleton.getInstance().mYBStyleConfig.getTitleTextColor());
                tv_indiana_details_title_bill.setTextColor(SessionSingleton.getInstance().mYBStyleConfig.getTitleTextColor());
                rl_indiana_details_background.setBackgroundColor(SessionSingleton.getInstance().mYBStyleConfig.getTitleBackColor());


                if (SessionSingleton.getInstance().mYBStyleConfig.getTitleBackIcon() == 0) {
                    iv_indiana_details_back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_back_black));
                } else {
                    iv_indiana_details_back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_back_write));
                }
            }


            ll_indiana_details_bottom.setVisibility(View.VISIBLE);
            if (surplusIntegral >= price) {
                tv_indiana_details_bottom_start.setBackground(getResources().getDrawable(R.drawable.orange_shape_c20));
                tv_indiana_details_bottom_start.setTextColor(getResources().getColor(R.color.yunbu_write));
                tv_indiana_details_bottom_start.setText("立即参与");
            } else {
                tv_indiana_details_bottom_start.setBackground(getResources().getDrawable(R.drawable.gray_shape_c20));
                tv_indiana_details_bottom_start.setTextColor(getResources().getColor(R.color.yunbu_textblack));
                tv_indiana_details_bottom_start.setText("活跃值不足，立即赚取");
            }


            joinStatus = 0;
            mLoading.show();
            page = 1;
            getIndianaDetailsMyList(page);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        xlv_indiana_details.setPullRefreshEnable(true);
        xlv_indiana_details.setPullLoadEnable(false);
        adapter = new IndianaDetailsAdapter(context);
        xlv_indiana_details.setXListViewListener(new XListView.IXListViewListener() {

            @Override
            public void onRefresh() {
                page = 1;
                mLoading.show();
                if (joinStatus == 0) {
                    getIndianaDetailsMyList(page);
                } else {
                    getIndianaDetailsList(page);
                }
                Load();
            }

            @Override
            public void onLoadMore() {
                page = page + 1;
                mLoading.show();
                if (joinStatus == 0) {
                    getIndianaDetailsMyList(page);
                } else {
                    getIndianaDetailsList(page);
                }
                Load();
            }

        });

        xlv_indiana_details.setAdapter(adapter);

    }


    private void setlistener() {
        iv_indiana_details_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tv_indiana_details_title_bill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(YunBuIndianaDetailsActivity.this, YunBuIndianaBillActivity.class);
                intent.putExtra("INDIANABILLTYPE", "duobao");
                startActivity(intent);
            }
        });

        tv_indiana_details_bottom_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (surplusIntegral < price) {
                    Intent intent = new Intent(YunBuIndianaDetailsActivity.this, YunBuIndianaMallActivity.class);
                    startActivity(intent);
                } else {
                    showTimes();
                }
            }
        });

        xlv_indiana_details.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                try {
                    SessionSingleton.getInstance().rewardDetailsSingle = detailsListArray.getJSONObject(position - 1);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void Load() {
        xlv_indiana_details.stopLoadMore();
        xlv_indiana_details.stopRefresh();
    }


    public class IndianaDetailsAdapter extends BaseAdapter {
        private AsyncImageLoader imageLoader;//异步组件
        private LayoutInflater inflater;


        public IndianaDetailsAdapter(Context context) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            MemoryCache mcache = new MemoryCache();//内存缓存
            String paht = getApplicationContext().getFilesDir().getAbsolutePath();
            File cacheDir = new File(paht, "yunbucache");//缓存根目录
            FileCache fcache = new FileCache(context, cacheDir, "yunbuimage");//文件缓存
            imageLoader = new AsyncImageLoader(context, mcache, fcache);
        }

        @Override
        public int getCount() {
            return detailsListArray.length() + 1;
        }

        @Override
        public Object getItem(int position) {

            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            if (position == 0) {
                return 0;
            } else {
                return 1;
            }
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            final ViewTopHolder topHolder;

            int type = getItemViewType(position);
            if (type == 0) {
                topHolder = new ViewTopHolder();
                convertView = inflater.inflate(R.layout.item_yun_bu_indiana_details_top, null);
                topHolder.rl_indiana_details_type = (RelativeLayout) convertView.findViewById(R.id.rl_indiana_details_type);
                topHolder.tv_indiana_details_number = (TextView) convertView.findViewById(R.id.tv_indiana_details_number);

                topHolder.tv_indiana_details_title = (TextView) convertView.findViewById(R.id.tv_indiana_details_top_title);
                topHolder.tv_indiana_details_price = (TextView) convertView.findViewById(R.id.tv_indiana_details_price);
                topHolder.pb_indiana_details_progress = (ProgressBar) convertView.findViewById(R.id.pb_indiana_details_progress);
                topHolder.tv_indiana_details_has_num = (TextView) convertView.findViewById(R.id.tv_indiana_details_has_num);
                topHolder.tv_indiana_details_surplus_num = (TextView) convertView.findViewById(R.id.tv_indiana_details_surplus_num);

                topHolder.tv_indiana_details_participation = (TextView) convertView.findViewById(R.id.tv_indiana_details_participation);
                topHolder.tv_indiana_details_previous = (TextView) convertView.findViewById(R.id.tv_indiana_details_previous);


                try {
                    if (joinStatus == 0) {
                        topHolder.tv_indiana_details_participation.setBackground(getResources().getDrawable(R.color.yunbu_lightgreen));
                        topHolder.tv_indiana_details_previous.setBackground(getResources().getDrawable(R.color.yunbu_write));

                        topHolder.tv_indiana_details_participation.setTextColor(getResources().getColor(R.color.yunbu_write));
                        topHolder.tv_indiana_details_previous.setTextColor(getResources().getColor(R.color.yunbu_textblack));
                    } else {

                        topHolder.tv_indiana_details_participation.setBackground(getResources().getDrawable(R.color.yunbu_write));
                        topHolder.tv_indiana_details_previous.setBackground(getResources().getDrawable(R.color.yunbu_lightgreen));

                        topHolder.tv_indiana_details_participation.setTextColor(getResources().getColor(R.color.yunbu_textblack));
                        topHolder.tv_indiana_details_previous.setTextColor(getResources().getColor(R.color.yunbu_write));
                    }


                    if (indianaType.equals("hongbao")) {
                        topHolder.rl_indiana_details_type.setBackground(getResources().getDrawable(R.mipmap.ic_yun_bu_indiana_details_redpacket));
                    } else {
                        topHolder.rl_indiana_details_type.setBackground(getResources().getDrawable(R.mipmap.ic_yun_bu_indiana_details_integral));
                    }

                    topHolder.tv_indiana_details_title.setText(detailsSingle.getString("goodsName"));
                    topHolder.tv_indiana_details_number.setText("期号：" + dateNumber);
                    topHolder.tv_indiana_details_price.setText(detailsSingle.getString("consumeScore") + "活跃值/次");
                    double hasnum = detailsSingle.getDouble("currentTimes") / detailsSingle.getDouble("needTimes") * 100;
                    topHolder.tv_indiana_details_has_num.setText("已参与" + (int) Math.floor(hasnum) + "%");
                    int ssurplusnum = detailsSingle.getInt("needTimes") - detailsSingle.getInt("currentTimes");
                    topHolder.tv_indiana_details_surplus_num.setText("剩余" + ssurplusnum + "次");
                    topHolder.pb_indiana_details_progress.setProgress(detailsSingle.getInt("currentTimes") * detailsSingle.getInt("needTimes") / 100);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                topHolder.tv_indiana_details_participation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        topHolder.tv_indiana_details_participation.setBackground(getResources().getDrawable(R.color.yunbu_lightgreen));
                        topHolder.tv_indiana_details_previous.setBackground(getResources().getDrawable(R.color.yunbu_write));

                        topHolder.tv_indiana_details_participation.setTextColor(getResources().getColor(R.color.yunbu_write));
                        topHolder.tv_indiana_details_previous.setTextColor(getResources().getColor(R.color.yunbu_textblack));

                        joinStatus = 0;
                        page = 1;
                        getIndianaDetailsMyList(page);
                    }
                });

                topHolder.tv_indiana_details_previous.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        topHolder.tv_indiana_details_participation.setBackground(getResources().getDrawable(R.color.yunbu_write));
                        topHolder.tv_indiana_details_previous.setBackground(getResources().getDrawable(R.color.yunbu_lightgreen));

                        topHolder.tv_indiana_details_participation.setTextColor(getResources().getColor(R.color.yunbu_textblack));
                        topHolder.tv_indiana_details_previous.setTextColor(getResources().getColor(R.color.yunbu_write));

                        joinStatus = 1;
                        page = 1;
                        getIndianaDetailsList(page);
                    }
                });
            } else {
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.item_yun_bu_indiana_details_item, null);
                holder.ll_indiana_details_item_bg = (LinearLayout) convertView.findViewById(R.id.ll_indiana_details_item_bg);
                holder.iv_indiana_details_item_head = (ImageView) convertView.findViewById(R.id.iv_indiana_details_item_head);
                holder.tv_indiana_details_item_title = (TextView) convertView.findViewById(R.id.tv_indiana_details_item_title);
                holder.tv_indiana_details_item_date = (TextView) convertView.findViewById(R.id.tv_indiana_details_item_date);
                holder.tv_indiana_details_item_times = (TextView) convertView.findViewById(R.id.tv_indiana_details_item_times);
                holder.tv_indiana_details_join_num = (TextView) convertView.findViewById(R.id.tv_indiana_details_join_num);

                try {
                    JSONObject single = detailsListArray.getJSONObject(position - 1);

                    if (SessionSingleton.getInstance().hasStyleConfig == 1) {
                        holder.ll_indiana_details_item_bg.setBackgroundColor(SessionSingleton.getInstance().mYBStyleConfig.getTitleBackColor());
                    }

                    holder.tv_indiana_details_item_date.setText(single.getString("duobaoData"));

                    if (joinStatus == 0) {
                        if(single.getString("channelUserId").equals(channelUserId)){
                            holder.tv_indiana_details_item_title.setTextColor(getResources().getColor(R.color.yunbu_textchecked));
                        }else{
                            holder.tv_indiana_details_item_title.setTextColor(getResources().getColor(R.color.yunbu_textblack));
                        }
                        holder.tv_indiana_details_item_title.setText("用户ID：" + single.getString("channelUserId"));

                        if (single.getString("beginNumber").equals(single.getString("endNumber"))) {
                            holder.tv_indiana_details_join_num.setText("参与号：" + single.getString("beginNumber"));
                        } else {
                            holder.tv_indiana_details_join_num.setText("参与号：" + single.getString("beginNumber") + "  -  " + single.getString("endNumber"));
                        }
                        holder.tv_indiana_details_item_times.setText("夺宝" + single.getString("joinTimes") + "次");
                    } else {

                        holder.tv_indiana_details_item_title.setText("期号：" + single.getString("dateNumber"));
                        holder.tv_indiana_details_join_num.setText("本期中奖：" + single.getString("rewardNumber"));
                        holder.tv_indiana_details_item_times.setText("夺宝" + single.getString("buyTimes") + "次");
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            return convertView;
        }

        class ViewTopHolder {
            RelativeLayout rl_indiana_details_type;
            TextView tv_indiana_details_number;

            TextView tv_indiana_details_title, tv_indiana_details_price;
            ProgressBar pb_indiana_details_progress;
            TextView tv_indiana_details_has_num, tv_indiana_details_surplus_num;

            TextView tv_indiana_details_participation, tv_indiana_details_previous;

        }

        class ViewHolder {
            TextView tv_indiana_details_item_title, tv_indiana_details_item_date, tv_indiana_details_item_times, tv_indiana_details_join_num;
            ImageView iv_indiana_details_item_head;
            LinearLayout ll_indiana_details_item_bg;
        }

    }


    //canyu
    private void JoinIndiana(int joinTimes) {
        Map<String, String> params = new HashMap<>();
        params.put("chanelUserAccount", chanelUserAccount);
        params.put("token", token);
        params.put("joinTimes", String.valueOf(joinTimes));
        params.put("id", productId);
        params.put("channelUserId", channelUserId);
        HttpUtils.doHttpReqeust("POST", SessionSingleton.getInstance().requestBaseUrl + "duobaoApi?", params, new HttpUtils.StringCallback() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject returnJSONObject = new JSONObject(response);
                    if (returnJSONObject.getString("status").equals("success")) {
                        timesPopuo.dismiss();
                        Intent intent = new Intent(YunBuIndianaDetailsActivity.this, YunBuIndianaSucceedActivity.class);
                        startActivity(intent);
                    } else {
                        Utils.showToast(context, returnJSONObject.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mLoading.dismiss();

            }

            @Override
            public void onFaileure(int code, Exception e) {
                mLoading.dismiss();
                e.printStackTrace();
            }
        });
    }

    //获取列表
    private void getIndianaDetailsMyList(final int pageindex) {
        Map<String, String> params = new HashMap<>();
        params.put("chanelUserAccount", chanelUserAccount);
        params.put("token", token);
        params.put("dateNumber", dateNumber);
        params.put("channelUserId", channelUserId);
        params.put("page", String.valueOf(pageindex));
        params.put("productId", productId);
        HttpUtils.doHttpReqeust("POST", SessionSingleton.getInstance().requestBaseUrl + "duobaoRearwardInfoApi?", params, new HttpUtils.StringCallback() {

            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject returnJSONObject = new JSONObject(response);
                    if (returnJSONObject.getString("status").equals("success")) {
                        ISORNOFIRST = 1;
                        JSONArray array = returnJSONObject.getJSONArray("data");
                        if (pageindex == 1) {
                            detailsListArray = array;
                        } else {
                            for (int i = 0; i < array.length(); i++) {
                                detailsListArray.put(array.getJSONObject(i));
                            }
                        }

                        detailsSingle = returnJSONObject.getJSONObject("productJSON");
                        dateNumber = detailsSingle.getString("dateNumber");
                        productId = detailsSingle.getString("id");

                        surplusIntegral = SessionSingleton.getInstance().WelfareSingle.getInt("score");
                        price = detailsSingle.getInt("consumeScore");
                        surplusTimes = detailsSingle.getInt("needTimes") - detailsSingle.getInt("currentTimes");

                        if (returnJSONObject.getString("newDate").equals("yes")) {
                            showFinish();
                        }

                        adapter.notifyDataSetChanged();
                    } else {
                        Utils.showToast(context, returnJSONObject.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mLoading.dismiss();

            }

            @Override
            public void onFaileure(int code, Exception e) {
                mLoading.dismiss();
                e.printStackTrace();

            }
        });
    }


    //获取往期列表
    private void getIndianaDetailsList(final int pageindex) {
        Map<String, String> params = new HashMap<>();
        params.put("chanelUserAccount", chanelUserAccount);
        params.put("token", token);
        params.put("dateNumber", dateNumber);
        params.put("channelUserId", channelUserId);
        params.put("page", String.valueOf(pageindex));
        params.put("productId", productId);
        HttpUtils.doHttpReqeust("POST", SessionSingleton.getInstance().requestBaseUrl + "duobaoRearwardWinApi?", params, new HttpUtils.StringCallback() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject returnJSONObject = new JSONObject(response);
                    if (returnJSONObject.getString("status").equals("success")) {

                        JSONArray array = returnJSONObject.getJSONArray("data");
                        if (pageindex == 1) {
                            detailsListArray = array;
                        } else {
                            for (int i = 0; i < array.length(); i++) {
                                detailsListArray.put(array.getJSONObject(i));
                            }
                        }
                        adapter.notifyDataSetChanged();
                    } else {
                        Utils.showToast(context, returnJSONObject.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mLoading.dismiss();

            }

            @Override
            public void onFaileure(int code, Exception e) {
                mLoading.dismiss();
                e.printStackTrace();

            }
        });
    }

    private void showTimes() {
        LayoutInflater la = LayoutInflater.from(context);
        View contentView = la.inflate(R.layout.pop_yun_bu_indiana_details_times_show, null);//自定义布局

        TextView tv_pop_indiana_details_reduce = contentView.findViewById(R.id.tv_pop_indiana_details_reduce);
        final TextView tv_pop_indiana_details_number = contentView.findViewById(R.id.tv_pop_indiana_details_number);
        TextView tv_pop_indiana_details_add = contentView.findViewById(R.id.tv_pop_indiana_details_add);

        TextView tv_pop_indiana_details_add_5 = contentView.findViewById(R.id.tv_pop_indiana_details_add_5);
        TextView tv_pop_indiana_details_add_10 = contentView.findViewById(R.id.tv_pop_indiana_details_add_10);
        TextView tv_pop_indiana_details_add_20 = contentView.findViewById(R.id.tv_pop_indiana_details_add_20);
        TextView tv_pop_indiana_details_add_50 = contentView.findViewById(R.id.tv_pop_indiana_details_add_50);
        TextView tv_pop_indiana_details_add_100 = contentView.findViewById(R.id.tv_pop_indiana_details_add_100);

        TextView tv_pop_indiana_details_price = contentView.findViewById(R.id.tv_pop_indiana_details_price);
        final TextView tv_pop_indiana_details_certain = contentView.findViewById(R.id.tv_pop_indiana_details_certain);

        tv_pop_indiana_details_price.setText("1次=" + price + "活跃值");
        tv_pop_indiana_details_certain.setText("消耗" + participateTimes * price + "活跃值，确认参与");

        tv_pop_indiana_details_certain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JoinIndiana(participateTimes);
            }
        });

        tv_pop_indiana_details_reduce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (participateTimes >= 2) {
                    participateTimes = participateTimes - 1;
                } else {
                    participateTimes = 1;
                }


                tv_pop_indiana_details_number.setText(String.valueOf(participateTimes));
                tv_pop_indiana_details_certain.setText("消耗" + participateTimes * price + "活跃值，确认参与");
            }
        });

        tv_pop_indiana_details_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (participateTimes < surplusTimes) {
                    participateTimes = participateTimes + 1;
                    if (surplusIntegral < participateTimes * price) {
                        participateTimes = participateTimes - 1;
                    }
                } else {
                    participateTimes = surplusTimes;
                }


                tv_pop_indiana_details_number.setText(String.valueOf(participateTimes));
                tv_pop_indiana_details_certain.setText("消耗" + participateTimes * price + "活跃值，确认参与");
            }
        });

        tv_pop_indiana_details_add_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (surplusIntegral < 5 * price) {
                    Utils.showToast(context, "您的活跃值只能参与" + (int) Math.floor(surplusIntegral / price) + "次夺宝");
                } else {
                    if (surplusTimes - 5 < 0) {
                        participateTimes = surplusTimes;
                    } else {
                        participateTimes = 5;
                    }
                    tv_pop_indiana_details_number.setText(String.valueOf(participateTimes));
                    tv_pop_indiana_details_certain.setText("消耗" + participateTimes * price + "活跃值，确认参与");
                }

            }
        });

        tv_pop_indiana_details_add_10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (surplusIntegral < 10 * price) {
                    Utils.showToast(context, "您的活跃值只能参与" + (int) Math.floor(surplusIntegral / price) + "次夺宝");
                } else {
                    if (surplusTimes - 10 < 0) {
                        participateTimes = surplusTimes;
                    } else {
                        participateTimes = 10;
                    }
                    tv_pop_indiana_details_number.setText(String.valueOf(participateTimes));
                    tv_pop_indiana_details_certain.setText("消耗" + participateTimes * price + "活跃值，确认参与");
                }

            }
        });

        tv_pop_indiana_details_add_20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (surplusIntegral < 20 * price) {
                    Utils.showToast(context, "您的活跃值只能参与" + (int) Math.floor(surplusIntegral / price) + "次夺宝");
                } else {
                    if (surplusTimes - 20 < 0) {
                        participateTimes = surplusTimes;
                    } else {
                        participateTimes = 20;
                    }
                    tv_pop_indiana_details_number.setText(String.valueOf(participateTimes));
                    tv_pop_indiana_details_certain.setText("消耗" + participateTimes * price + "活跃值，确认参与");
                }
            }
        });

        tv_pop_indiana_details_add_50.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (surplusIntegral < 50 * price) {
                    Utils.showToast(context, "您的活跃值只能参与" + (int) Math.floor(surplusIntegral / price) + "次夺宝");
                } else {
                    if (surplusTimes - 50 < 0) {
                        participateTimes = surplusTimes;
                    } else {
                        participateTimes = 50;
                    }
                    tv_pop_indiana_details_number.setText(String.valueOf(participateTimes));
                    tv_pop_indiana_details_certain.setText("消耗" + participateTimes * price + "活跃值，确认参与");
                }
            }
        });

        tv_pop_indiana_details_add_100.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (surplusIntegral < 100 * price) {
                    Utils.showToast(context, "您的活跃值只能参与" + (int) Math.floor(surplusIntegral / price) + "次夺宝");
                } else {
                    if (surplusTimes - 100 < 0) {
                        participateTimes = surplusTimes;
                    } else {
                        participateTimes = 100;
                    }
                    tv_pop_indiana_details_number.setText(String.valueOf(participateTimes));
                    tv_pop_indiana_details_certain.setText("消耗" + participateTimes * price + "活跃值，确认参与");
                }
            }
        });

        contentView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                timesPopuo.dismiss();
            }
        });


        timesPopuo = new PopupWindow(contentView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);


        //设置PopupWindow的焦点
        timesPopuo.setFocusable(true);
        timesPopuo.setClippingEnabled(false);
        //点击PopupWindow之外的地方PopupWindow会消失
        timesPopuo.setOutsideTouchable(true);
        //showAtLocation(View parent, int gravity, int x, int y)：相对于父控件的位置（例如正中央Gravity.CENTER，下方Gravity.BOTTOM等），可以设置偏移或无偏移
        timesPopuo.showAtLocation(this.getWindow().getDecorView(), Gravity.BOTTOM, 0, 0);
        timesPopuo.update();
    }

    //benqijieshu
    private void showFinish() {

        builder = new AlertDialog.Builder(YunBuIndianaDetailsActivity.this);//创建对话框

        inflater = getLayoutInflater();
        layout = inflater.inflate(R.layout.pop_yun_bu_finish, null);//获取自定义布局
        builder.setView(layout);//设置对话框的布局
        dialog = builder.create();//生成最终的对话框

        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.gravity = Gravity.BOTTOM;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;

        dialog.getWindow().setAttributes(params);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        countTimer = new CountTimer(1 * 1000 + 500, 500);
        countTimer.start();

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        CountDownTimer timer = new CountDownTimer(3000, 10) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                dialog.dismiss();
            }
        };


    }


    private class CountTimer extends CountDownTimer {
        public CountTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            cancel();
        }

        @Override
        public void onTick(long millisUntilFinished) {
            if (millisUntilFinished < 1000 && dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (ISORNOFIRST != 0) {
            joinStatus = 0;
            mLoading.show();
            page = 1;
            getIndianaDetailsMyList(page);
        }
    }
}
