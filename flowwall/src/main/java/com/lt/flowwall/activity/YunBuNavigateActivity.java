package com.lt.flowwall.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kwad.sdk.api.KsAdSDK;
import com.kwad.sdk.api.SdkConfig;
import com.lt.flowwall.R;
import com.lt.flowwall.fragment.GameFragment;
import com.lt.flowwall.fragment.PartInFragment;
import com.lt.flowwall.fragment.RewardFragment;
import com.lt.flowwall.fragment.YunBuWelfareFragment;
import com.lt.flowwall.tools.TTAdManagerHolder;
import com.lt.flowwall.tools.TabPageAdapter;
import com.lt.flowwall.tools.customViewPager;
import com.lt.flowwall.utils.SessionSingleton;
import com.qq.e.comm.managers.GDTAdSdk;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class YunBuNavigateActivity extends AppCompatActivity implements View.OnClickListener {
    private LinearLayout ll_yunbu_game, ll_yunbu_reward, ll_yunbu_partin, ll_yunbu_welfare;
    private LinearLayout ll_navigation_game_bgcolor, ll_navigation_reward_bgcolor, ll_navigation_partin_bgcolor, ll_navigation_welfare_bgcolor;
    private ImageView iv_game, iv_reward, iv_partin, iv_welfare;
    private TextView tv_game, tv_reward, tv_partin, tv_welfare;

    private ImageView iv_main_back;
    private TextView tv_navigate_title, tv_navigate_title_withdrawal;
    private RelativeLayout rl_main_background;

    private customViewPager viewpager;
    int[] unselectedIconIds;
    int[] selectedIconIds;
    List<Fragment> fragments = new ArrayList<Fragment>();

    private int showTypeGame = 0, showTypeReward = 0;

    private String withdrawType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        if (Build.VERSION.SDK_INT >= 21) {
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            //decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        setContentView(R.layout.activity_yun_bu_navigate);

        if (!SessionSingleton.getInstance().guangdiantong.equals("none")) {
            //广点通广告
            GDTAdSdk.init(this, SessionSingleton.getInstance().gdtId);
        }

        if (!SessionSingleton.getInstance().guangdiantong.equals("none")) {
            // 穿山甲SDK初始化
            // 强烈建议在应用对应的Application#onCreate()方法中调用，避免出现content为null的异常
            TTAdManagerHolder.init(this);
        }

        if (!SessionSingleton.getInstance().kuaishou.equals("none")) {
            String currentProcessName = getCurrentProcessName();
            if (currentProcessName.equals(getPackageName())) {
                // 建议只在需要的进程初始化SDK即可，如主进程
                KsAdSDK.init(this, new SdkConfig.Builder()
                        .appId(SessionSingleton.getInstance().ksId) // 测试aapId，请联系快手平台申请正式AppId，必填
                        .showNotification(true) // 是否展示下载通知栏
                        .debug(true)
                        .build());
            }
        }

        initview();

    }


    private String getCurrentProcessName() {
        int pid = Process.myPid();
        String currentProcessName = "";
        ActivityManager activityManager = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = activityManager.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo processInfo : runningAppProcesses) {
            if (pid == processInfo.pid) {
                currentProcessName = processInfo.processName;
            }
        }
        return currentProcessName;
    }


    private void initview() {

        ll_yunbu_game = findViewById(R.id.ll_yunbu_game);
        ll_yunbu_partin = findViewById(R.id.ll_yunbu_partin);
        ll_yunbu_reward = findViewById(R.id.ll_yunbu_reward);
        ll_yunbu_welfare = findViewById(R.id.ll_yunbu_welfare);

        ll_navigation_game_bgcolor = findViewById(R.id.ll_navigation_game_bgcolor);
        ll_navigation_reward_bgcolor = findViewById(R.id.ll_navigation_reward_bgcolor);
        ll_navigation_partin_bgcolor = findViewById(R.id.ll_navigation_partin_bgcolor);
        ll_navigation_welfare_bgcolor = findViewById(R.id.ll_navigation_welfare_bgcolor);

        iv_game = findViewById(R.id.homeimage);
        iv_reward = findViewById(R.id.rewardimage);
        iv_partin = findViewById(R.id.partinimage);
        iv_welfare = findViewById(R.id.welfareimage);

        tv_game = findViewById(R.id.hometext);
        tv_reward = findViewById(R.id.rewardtext);
        tv_partin = findViewById(R.id.partintext);
        tv_welfare = findViewById(R.id.welfaretext);

        iv_main_back = findViewById(R.id.iv_main_back);
        tv_navigate_title = findViewById(R.id.tv_navigate_title);
        tv_navigate_title_withdrawal = findViewById(R.id.tv_navigate_title_withdrawal);
        rl_main_background = findViewById(R.id.rl_main_background);

        if (SessionSingleton.getInstance().hasStyleConfig == 1) {
            tv_navigate_title.setText(SessionSingleton.getInstance().mYBStyleConfig.getTitleText());
            tv_navigate_title.setTextColor(SessionSingleton.getInstance().mYBStyleConfig.getTitleTextColor());
            rl_main_background.setBackgroundColor(SessionSingleton.getInstance().mYBStyleConfig.getTitleBackColor());

            if (SessionSingleton.getInstance().mYBStyleConfig.getTitleBackIcon() == 0) {
                iv_main_back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_back_black));
            } else {
                iv_main_back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_back_write));
            }

            ll_navigation_game_bgcolor.setBackgroundColor(SessionSingleton.getInstance().mYBStyleConfig.getNavigateTextColor());
            ll_navigation_reward_bgcolor.setBackgroundColor(SessionSingleton.getInstance().mYBStyleConfig.getNavigateTextColor());
            ll_navigation_partin_bgcolor.setBackgroundColor(SessionSingleton.getInstance().mYBStyleConfig.getNavigateTextColor());
            ll_navigation_welfare_bgcolor.setBackgroundColor(SessionSingleton.getInstance().mYBStyleConfig.getNavigateTextColor());
        }


        ll_yunbu_game.setOnClickListener(this);
        ll_yunbu_reward.setOnClickListener(this);
        ll_yunbu_partin.setOnClickListener(this);
        ll_yunbu_welfare.setOnClickListener(this);

        try {
            String showTabSign = SessionSingleton.getInstance().AccountSingle.getString("showTabSign");

            withdrawType = SessionSingleton.getInstance().AccountSingle.getString("withdrawType");

            //todo
            //showTabSign = "游戏";
            if (showTabSign.contains("悬赏")) {
                showTypeReward = 1;
            }

            if (showTabSign.contains("游戏")) {
                showTypeGame = 1;
            }

            if (withdrawType.equals("平台自提")) {
                if (SessionSingleton.getInstance().hasStyleConfig == 1) {
                    tv_navigate_title_withdrawal.setVisibility(View.GONE);
                } else {
                    tv_navigate_title_withdrawal.setVisibility(View.GONE);
                }
            } else {
                if (SessionSingleton.getInstance().hasStyleConfig == 1) {
                    tv_navigate_title_withdrawal.setVisibility(View.VISIBLE);
                    tv_navigate_title_withdrawal.setTextColor(SessionSingleton.getInstance().mYBStyleConfig.getTitleTextColor());
                } else {
                    tv_navigate_title_withdrawal.setVisibility(View.VISIBLE);
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


        checkNeedPermissions();

        tv_navigate_title_withdrawal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(YunBuNavigateActivity.this, YunBuWithdrawalActivity.class);
                startActivity(intent);
            }
        });

        iv_main_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    @Override
    public void onClick(View v) {

        int id = v.getId();
        if (id == R.id.ll_yunbu_game) {
            iv_game.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_game_select));
            iv_welfare.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_welfare_unselect));
            iv_reward.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_reward_unselect));
            iv_partin.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_partin_unselect));


            if (SessionSingleton.getInstance().hasStyleConfig == 1) {
                tv_game.setTextColor(SessionSingleton.getInstance().mYBStyleConfig.getNavigateTextColor());
            } else {
                tv_game.setTextColor(getResources().getColor(R.color.yunbu_green));
            }
            tv_welfare.setTextColor(getResources().getColor(R.color.yunbu_textgray));
            tv_reward.setTextColor(getResources().getColor(R.color.yunbu_textgray));
            tv_partin.setTextColor(getResources().getColor(R.color.yunbu_textgray));

            if (showTypeReward == 1 && showTypeGame == 1) {
                viewpager.setCurrentItem(0, false);
            } else if (showTypeReward == 1 && showTypeGame != 1) {
                viewpager.setCurrentItem(0, false);
            } else if (showTypeReward != 1 && showTypeGame == 1) {
                viewpager.setCurrentItem(0, false);
            }

        } else if (id == R.id.ll_yunbu_welfare) {
            iv_game.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_game_unselect));
            iv_welfare.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_welfare_select));
            iv_reward.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_reward_unselect));
            iv_partin.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_partin_unselect));

            if (SessionSingleton.getInstance().hasStyleConfig == 1) {
                tv_welfare.setTextColor(SessionSingleton.getInstance().mYBStyleConfig.getNavigateTextColor());
            } else {
                tv_welfare.setTextColor(getResources().getColor(R.color.yunbu_green));
            }
            tv_game.setTextColor(getResources().getColor(R.color.yunbu_textgray));
            tv_reward.setTextColor(getResources().getColor(R.color.yunbu_textgray));
            tv_partin.setTextColor(getResources().getColor(R.color.yunbu_textgray));

            if (showTypeReward == 1 && showTypeGame == 1) {
                viewpager.setCurrentItem(1, false);
            } else if (showTypeReward == 1 && showTypeGame != 1) {
                viewpager.setCurrentItem(1, false);
            } else if (showTypeReward != 1 && showTypeGame == 1) {
                viewpager.setCurrentItem(1, false);
            }

        } else if (id == R.id.ll_yunbu_reward) {
            iv_game.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_game_unselect));
            iv_welfare.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_welfare_unselect));
            iv_reward.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_reward_select));
            iv_partin.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_partin_unselect));

            tv_welfare.setTextColor(getResources().getColor(R.color.yunbu_textgray));
            tv_game.setTextColor(getResources().getColor(R.color.yunbu_textgray));
            if (SessionSingleton.getInstance().hasStyleConfig == 1) {
                tv_reward.setTextColor(SessionSingleton.getInstance().mYBStyleConfig.getNavigateTextColor());
            } else {
                tv_reward.setTextColor(getResources().getColor(R.color.yunbu_green));
            }
            tv_partin.setTextColor(getResources().getColor(R.color.yunbu_textgray));

            if (showTypeReward == 1 && showTypeGame == 1) {
                viewpager.setCurrentItem(2, false);
            } else if (showTypeReward == 1 && showTypeGame != 1) {
                viewpager.setCurrentItem(1, false);
            } else if (showTypeReward != 1 && showTypeGame == 1) {
                viewpager.setCurrentItem(1, false);
            }
        } else if (id == R.id.ll_yunbu_partin) {
            iv_game.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_game_unselect));
            iv_welfare.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_welfare_unselect));
            iv_reward.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_reward_unselect));
            iv_partin.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_partin_select));

            tv_welfare.setTextColor(getResources().getColor(R.color.yunbu_textgray));
            tv_game.setTextColor(getResources().getColor(R.color.yunbu_textgray));
            tv_reward.setTextColor(getResources().getColor(R.color.yunbu_textgray));
            if (SessionSingleton.getInstance().hasStyleConfig == 1) {
                tv_partin.setTextColor(SessionSingleton.getInstance().mYBStyleConfig.getNavigateTextColor());
            } else {
                tv_partin.setTextColor(getResources().getColor(R.color.yunbu_green));
            }

            if (showTypeReward == 1 && showTypeGame == 1) {
                viewpager.setCurrentItem(3, false);
            } else if (showTypeReward == 1 && showTypeGame != 1) {
                viewpager.setCurrentItem(2, false);
            } else if (showTypeReward != 1 && showTypeGame == 1) {
                viewpager.setCurrentItem(2, false);
            }
        }

    }

    private void checkNeedPermissions() {
        String[] PERMISSIONS_STORAGE = {
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        };
        int permission = ActivityCompat.checkSelfPermission(YunBuNavigateActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    YunBuNavigateActivity.this,
                    PERMISSIONS_STORAGE,
                    1
            );
        } else {

            if (showTypeReward == 1 && showTypeGame == 1) {
                unselectedIconIds = new int[]{R.mipmap.ic_yunbu_welfare_unselect, R.mipmap.ic_yunbu_game_unselect, R.mipmap.ic_yunbu_reward_unselect, R.mipmap.ic_yunbu_partin_unselect};
                selectedIconIds = new int[]{R.mipmap.ic_yunbu_welfare_select, R.mipmap.ic_yunbu_game_select, R.mipmap.ic_yunbu_reward_select, R.mipmap.ic_yunbu_partin_select};

                //默认选中首页
                iv_game.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_game_select));

                if (SessionSingleton.getInstance().hasStyleConfig == 1) {
                    tv_game.setTextColor(SessionSingleton.getInstance().mYBStyleConfig.getNavigateTextColor());
                } else {
                    tv_game.setTextColor(getResources().getColor(R.color.yunbu_green));
                }
                tv_welfare.setTextColor(getResources().getColor(R.color.yunbu_textgray));
                tv_reward.setTextColor(getResources().getColor(R.color.yunbu_textgray));
                tv_partin.setTextColor(getResources().getColor(R.color.yunbu_textgray));

                ll_yunbu_welfare.setVisibility(View.VISIBLE);
                ll_yunbu_game.setVisibility(View.VISIBLE);
                ll_yunbu_reward.setVisibility(View.VISIBLE);
                ll_yunbu_partin.setVisibility(View.VISIBLE);

                //将fragment放入范型
                Fragment GameFragment = new GameFragment();
                Fragment YunBuWelfareFragment = new YunBuWelfareFragment();
                Fragment RewardFragment = new RewardFragment();
                Fragment PartInFragment = new PartInFragment();


                fragments.add(GameFragment);
                fragments.add(YunBuWelfareFragment);
                fragments.add(RewardFragment);
                fragments.add(PartInFragment);


                viewpager = findViewById(R.id.home_viewpager);
                viewpager.setPagingEnabled(false);
                TabPageAdapter tabPageAdapter = new TabPageAdapter(getSupportFragmentManager(), fragments);
                viewpager.setAdapter(tabPageAdapter);

                fragments = null;
            } else if (showTypeReward == 1 && showTypeGame != 1) {

                unselectedIconIds = new int[]{R.mipmap.ic_yunbu_welfare_unselect, R.mipmap.ic_yunbu_reward_unselect, R.mipmap.ic_yunbu_partin_unselect};
                selectedIconIds = new int[]{R.mipmap.ic_yunbu_welfare_select, R.mipmap.ic_yunbu_reward_select, R.mipmap.ic_yunbu_partin_select};

                //默认选中首页
                iv_welfare.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_welfare_select));

                if (SessionSingleton.getInstance().hasStyleConfig == 1) {
                    tv_welfare.setTextColor(SessionSingleton.getInstance().mYBStyleConfig.getNavigateTextColor());
                } else {
                    tv_welfare.setTextColor(getResources().getColor(R.color.yunbu_green));
                }
                tv_game.setTextColor(getResources().getColor(R.color.yunbu_textgray));
                tv_reward.setTextColor(getResources().getColor(R.color.yunbu_textgray));
                tv_partin.setTextColor(getResources().getColor(R.color.yunbu_textgray));

                ll_yunbu_welfare.setVisibility(View.VISIBLE);
                ll_yunbu_game.setVisibility(View.GONE);
                ll_yunbu_reward.setVisibility(View.VISIBLE);
                ll_yunbu_partin.setVisibility(View.VISIBLE);

                //将fragment放入范型
                Fragment YunBuWelfareFragment = new YunBuWelfareFragment();
                Fragment RewardFragment = new RewardFragment();
                Fragment PartInFragment = new PartInFragment();

                fragments.add(YunBuWelfareFragment);
                fragments.add(RewardFragment);
                fragments.add(PartInFragment);


                viewpager = findViewById(R.id.home_viewpager);
                viewpager.setPagingEnabled(false);
                TabPageAdapter tabPageAdapter = new TabPageAdapter(
                        getSupportFragmentManager(), fragments);
                viewpager.setAdapter(tabPageAdapter);

                fragments = null;
            } else if (showTypeReward != 1 && showTypeGame == 1) {

                unselectedIconIds = new int[]{R.mipmap.ic_yunbu_welfare_unselect, R.mipmap.ic_yunbu_game_unselect, R.mipmap.ic_yunbu_partin_unselect};
                selectedIconIds = new int[]{R.mipmap.ic_yunbu_welfare_select, R.mipmap.ic_yunbu_game_select, R.mipmap.ic_yunbu_partin_select};

                //默认选中首页
                iv_game.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_game_select));

                if (SessionSingleton.getInstance().hasStyleConfig == 1) {
                    tv_game.setTextColor(SessionSingleton.getInstance().mYBStyleConfig.getNavigateTextColor());
                } else {
                    tv_game.setTextColor(getResources().getColor(R.color.yunbu_green));
                }
                tv_welfare.setTextColor(getResources().getColor(R.color.yunbu_textgray));
                tv_reward.setTextColor(getResources().getColor(R.color.yunbu_textgray));
                tv_partin.setTextColor(getResources().getColor(R.color.yunbu_textgray));

                ll_yunbu_welfare.setVisibility(View.VISIBLE);
                ll_yunbu_game.setVisibility(View.VISIBLE);
                ll_yunbu_reward.setVisibility(View.GONE);
                ll_yunbu_partin.setVisibility(View.VISIBLE);

                //将fragment放入范型
                Fragment GameFragment = new GameFragment();
                Fragment YunBuWelfareFragment = new YunBuWelfareFragment();
                Fragment PartInFragment = new PartInFragment();


                fragments.add(GameFragment);
                fragments.add(YunBuWelfareFragment);
                fragments.add(PartInFragment);


                viewpager = findViewById(R.id.home_viewpager);
                viewpager.setPagingEnabled(false);
                TabPageAdapter tabPageAdapter = new TabPageAdapter(
                        getSupportFragmentManager(), fragments);
                viewpager.setAdapter(tabPageAdapter);

                fragments = null;
            }


        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


            if (showTypeReward == 1 && showTypeGame == 1) {
                unselectedIconIds = new int[]{R.mipmap.ic_yunbu_welfare_unselect, R.mipmap.ic_yunbu_game_unselect, R.mipmap.ic_yunbu_reward_unselect, R.mipmap.ic_yunbu_partin_unselect};
                selectedIconIds = new int[]{R.mipmap.ic_yunbu_welfare_select, R.mipmap.ic_yunbu_game_select, R.mipmap.ic_yunbu_reward_select, R.mipmap.ic_yunbu_partin_select};

                //默认选中首页
                iv_game.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_game_select));

                if (SessionSingleton.getInstance().hasStyleConfig == 1) {
                    tv_game.setTextColor(SessionSingleton.getInstance().mYBStyleConfig.getNavigateTextColor());
                } else {
                    tv_game.setTextColor(getResources().getColor(R.color.yunbu_green));
                }
                tv_welfare.setTextColor(getResources().getColor(R.color.yunbu_textgray));
                tv_reward.setTextColor(getResources().getColor(R.color.yunbu_textgray));
                tv_partin.setTextColor(getResources().getColor(R.color.yunbu_textgray));

                ll_yunbu_welfare.setVisibility(View.VISIBLE);
                ll_yunbu_game.setVisibility(View.VISIBLE);
                ll_yunbu_reward.setVisibility(View.VISIBLE);
                ll_yunbu_partin.setVisibility(View.VISIBLE);

                //将fragment放入范型
                Fragment GameFragment = new GameFragment();
                Fragment YunBuWelfareFragment = new YunBuWelfareFragment();
                Fragment RewardFragment = new RewardFragment();
                Fragment PartInFragment = new PartInFragment();

                fragments.add(YunBuWelfareFragment);
                fragments.add(GameFragment);
                fragments.add(RewardFragment);
                fragments.add(PartInFragment);


                viewpager = findViewById(R.id.home_viewpager);
                viewpager.setPagingEnabled(false);
                TabPageAdapter tabPageAdapter = new TabPageAdapter(getSupportFragmentManager(), fragments);
                viewpager.setAdapter(tabPageAdapter);

                fragments = null;
            } else if (showTypeReward == 1 && showTypeGame != 1) {

                unselectedIconIds = new int[]{R.mipmap.ic_yunbu_welfare_unselect, R.mipmap.ic_yunbu_reward_unselect, R.mipmap.ic_yunbu_partin_unselect};
                selectedIconIds = new int[]{R.mipmap.ic_yunbu_welfare_select, R.mipmap.ic_yunbu_reward_select, R.mipmap.ic_yunbu_partin_select};

                //默认选中首页
                iv_welfare.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_reward_select));


                if (SessionSingleton.getInstance().hasStyleConfig == 1) {
                    tv_welfare.setTextColor(SessionSingleton.getInstance().mYBStyleConfig.getNavigateTextColor());
                } else {
                    tv_welfare.setTextColor(getResources().getColor(R.color.yunbu_green));
                }
                tv_game.setTextColor(getResources().getColor(R.color.yunbu_textgray));
                tv_reward.setTextColor(getResources().getColor(R.color.yunbu_textgray));
                tv_partin.setTextColor(getResources().getColor(R.color.yunbu_textgray));

                ll_yunbu_welfare.setVisibility(View.VISIBLE);
                ll_yunbu_game.setVisibility(View.GONE);
                ll_yunbu_reward.setVisibility(View.VISIBLE);
                ll_yunbu_partin.setVisibility(View.VISIBLE);

                //将fragment放入范型
                Fragment YunBuWelfareFragment = new YunBuWelfareFragment();
                Fragment RewardFragment = new RewardFragment();
                Fragment PartInFragment = new PartInFragment();

                fragments.add(YunBuWelfareFragment);
                fragments.add(RewardFragment);
                fragments.add(PartInFragment);


                viewpager = findViewById(R.id.home_viewpager);
                viewpager.setPagingEnabled(false);
                TabPageAdapter tabPageAdapter = new TabPageAdapter(
                        getSupportFragmentManager(), fragments);
                viewpager.setAdapter(tabPageAdapter);

                fragments = null;
            } else if (showTypeReward != 1 && showTypeGame == 1) {

                unselectedIconIds = new int[]{R.mipmap.ic_yunbu_welfare_unselect, R.mipmap.ic_yunbu_game_unselect, R.mipmap.ic_yunbu_partin_unselect};
                selectedIconIds = new int[]{R.mipmap.ic_yunbu_welfare_select, R.mipmap.ic_yunbu_game_select, R.mipmap.ic_yunbu_partin_select};

                //默认选中首页
                //默认选中首页
                iv_game.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_game_select));

                if (SessionSingleton.getInstance().hasStyleConfig == 1) {
                    tv_game.setTextColor(SessionSingleton.getInstance().mYBStyleConfig.getNavigateTextColor());
                } else {
                    tv_game.setTextColor(getResources().getColor(R.color.yunbu_green));
                }
                tv_welfare.setTextColor(getResources().getColor(R.color.yunbu_textgray));
                tv_reward.setTextColor(getResources().getColor(R.color.yunbu_textgray));
                tv_partin.setTextColor(getResources().getColor(R.color.yunbu_textgray));

                ll_yunbu_welfare.setVisibility(View.VISIBLE);
                ll_yunbu_game.setVisibility(View.VISIBLE);
                ll_yunbu_reward.setVisibility(View.GONE);
                ll_yunbu_partin.setVisibility(View.VISIBLE);

                //将fragment放入范型
                Fragment GameFragment = new GameFragment();
                Fragment YunBuWelfareFragment = new YunBuWelfareFragment();
                Fragment PartInFragment = new PartInFragment();

                fragments.add(GameFragment);
                fragments.add(YunBuWelfareFragment);
                fragments.add(PartInFragment);


                viewpager = findViewById(R.id.home_viewpager);
                viewpager.setPagingEnabled(false);
                TabPageAdapter tabPageAdapter = new TabPageAdapter(
                        getSupportFragmentManager(), fragments);
                viewpager.setAdapter(tabPageAdapter);

                fragments = null;
            }

        } else {
            // 没有获取 到权限，从新请求，或者关闭app
            Toast.makeText(this, "需要存储权限", Toast.LENGTH_SHORT).show();

            finish();
        }
    }

    @Override
    protected void onResume() {
        int id = SessionSingleton.getInstance().backFragemntNum;
        if (id == 1) {
            Fragment GameFragment = new GameFragment();
            FragmentManager fmanger = getSupportFragmentManager();
            FragmentTransaction transaction = fmanger.beginTransaction();
            transaction.replace(R.id.home_viewpager, GameFragment);
            transaction.addToBackStack(null);
            transaction.commit();

            if (showTypeReward == 1 && showTypeGame == 1) {
                viewpager.setCurrentItem(0, false);
            } else if (showTypeReward == 1 && showTypeGame != 1) {
                viewpager.setCurrentItem(0, false);
            } else if (showTypeReward != 1 && showTypeGame == 1) {
                viewpager.setCurrentItem(0, false);
            }

            //帮助跳转到指定子fragment
            Intent i = new Intent();
            i.setClass(YunBuNavigateActivity.this, GameFragment.class);


            iv_game.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_game_select));
            iv_welfare.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_welfare_unselect));
            iv_reward.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_reward_unselect));
            iv_partin.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_partin_unselect));


            if (SessionSingleton.getInstance().hasStyleConfig == 1) {
                tv_game.setTextColor(SessionSingleton.getInstance().mYBStyleConfig.getNavigateTextColor());
            } else {
                tv_game.setTextColor(getResources().getColor(R.color.yunbu_green));
            }
            tv_welfare.setTextColor(getResources().getColor(R.color.yunbu_textgray));
            tv_reward.setTextColor(getResources().getColor(R.color.yunbu_textgray));
            tv_partin.setTextColor(getResources().getColor(R.color.yunbu_textgray));


            SessionSingleton.getInstance().backFragemntNum = 3;

        } else if (id == 2) {
            Fragment YunBuWelfareFragment = new YunBuWelfareFragment();
            FragmentManager fmanger = getSupportFragmentManager();
            FragmentTransaction transaction = fmanger.beginTransaction();
            transaction.replace(R.id.home_viewpager, YunBuWelfareFragment);
            transaction.addToBackStack(null);
            transaction.commit();


            if (showTypeReward == 1 && showTypeGame == 1) {
                viewpager.setCurrentItem(1, false);
            } else if (showTypeReward == 1 && showTypeGame != 1) {
                viewpager.setCurrentItem(1, false);
            } else if (showTypeReward != 1 && showTypeGame == 1) {
                viewpager.setCurrentItem(1, false);
            }

            //帮助跳转到指定子fragment
            Intent i = new Intent();
            i.setClass(YunBuNavigateActivity.this, YunBuWelfareFragment.class);


            iv_game.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_game_unselect));
            iv_welfare.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_welfare_select));
            iv_reward.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_reward_unselect));
            iv_partin.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_partin_unselect));

            if (SessionSingleton.getInstance().hasStyleConfig == 1) {
                tv_welfare.setTextColor(SessionSingleton.getInstance().mYBStyleConfig.getNavigateTextColor());
            } else {
                tv_welfare.setTextColor(getResources().getColor(R.color.yunbu_green));
            }
            tv_game.setTextColor(getResources().getColor(R.color.yunbu_textgray));
            tv_reward.setTextColor(getResources().getColor(R.color.yunbu_textgray));
            tv_partin.setTextColor(getResources().getColor(R.color.yunbu_textgray));


            SessionSingleton.getInstance().backFragemntNum = 3;
        }
        super.onResume();
    }
}
