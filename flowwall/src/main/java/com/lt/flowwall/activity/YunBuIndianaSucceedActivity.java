package com.lt.flowwall.activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lt.flowwall.R;
import com.lt.flowwall.utils.SessionSingleton;

public class YunBuIndianaSucceedActivity extends AppCompatActivity {
    private Context context;
    public Dialog mLoading;

    private ImageView iv_indiana_succeed_back;
    private TextView tv_indiana_succeed_title;
    private RelativeLayout rl_indiana_succeed_background;

    private TextView tv_indiana_succeed_details, tv_indiana_succeed_continue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        if (Build.VERSION.SDK_INT >= 21) {
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        setContentView(R.layout.activity_yun_bu_indiana_succeed);

        context = this;

        innitview();
        setlistener();
    }

    private void innitview() {
        iv_indiana_succeed_back = findViewById(R.id.iv_indiana_succeed_back);
        tv_indiana_succeed_title = findViewById(R.id.tv_indiana_succeed_title);
        rl_indiana_succeed_background = findViewById(R.id.rl_indiana_succeed_background);

        tv_indiana_succeed_details = findViewById(R.id.tv_indiana_succeed_details);
        tv_indiana_succeed_continue = findViewById(R.id.tv_indiana_succeed_continue);

        if (SessionSingleton.getInstance().hasStyleConfig == 1) {
            tv_indiana_succeed_title.setTextColor(SessionSingleton.getInstance().mYBStyleConfig.getTitleTextColor());
            rl_indiana_succeed_background.setBackgroundColor(SessionSingleton.getInstance().mYBStyleConfig.getTitleBackColor());


            if (SessionSingleton.getInstance().mYBStyleConfig.getTitleBackIcon() == 0) {
                iv_indiana_succeed_back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_back_black));
            } else {
                iv_indiana_succeed_back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_yunbu_back_write));
            }
        }
    }

    private void setlistener() {
        iv_indiana_succeed_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(YunBuIndianaSucceedActivity.this, YunBuIndianaMallActivity.class);
                startActivity(intent);
                //finish();
            }
        });

        tv_indiana_succeed_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(YunBuIndianaSucceedActivity.this, YunBuIndianaBillActivity.class);
                intent.putExtra("INDIANABILLTYPE", "duobao");
                startActivity(intent);
                //finish();
            }
        });

        tv_indiana_succeed_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(YunBuIndianaSucceedActivity.this, YunBuIndianaMallActivity.class);
                startActivity(intent);
            }
        });
    }
}
