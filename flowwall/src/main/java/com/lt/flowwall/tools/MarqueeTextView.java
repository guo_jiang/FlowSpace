
package com.lt.flowwall.tools;


import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.lt.flowwall.R;
import com.lt.flowwall.utils.SessionSingleton;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Random;


public class MarqueeTextView extends FrameLayout {
    private JSONArray tipList;
    private int curTipIndex = 0;
    private long lastTimeMillis;
    private static final int ANIM_DELAYED_MILLIONS = 3 * 1000;
    /**
     * 动画持续时长
     */
    private static final int ANIM_DURATION = 1 * 1000;

    private static final int DEFAULT_TEXT_SIZE = 12;
    private Drawable head_boy, head_girl;
    private TextView tv_tip_out, tv_tip_in;

    private Animation anim_out, anim_in;

    public MarqueeTextView(Context context) {
        super(context);
        initTipFrame();
        initAnimation();
    }

    public MarqueeTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initTipFrame();
        initAnimation();
    }

    public MarqueeTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initTipFrame();
        initAnimation();
    }

    private void initTipFrame() {
        head_boy = loadDrawable(R.drawable.ic_loading);
        head_girl = loadDrawable(R.drawable.ic_loading);
        tv_tip_out = newTextView();
        tv_tip_in = newTextView();
        addView(tv_tip_in);
        addView(tv_tip_out);
    }

    private TextView newTextView() {
        TextView textView = new TextView(getContext());
        LayoutParams lp = new LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, Gravity.CENTER);
        textView.setLayoutParams(lp);
        textView.setCompoundDrawablePadding(15);
        textView.setGravity(Gravity.CENTER);
        textView.setLines(2);
        textView.setEllipsize(TextUtils.TruncateAt.END);
        Resources resource = getResources();
        ColorStateList csl = resource.getColorStateList(R.color.yunbu_textchecked);


        textView.setTextColor(csl);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_TEXT_SIZE);
        return textView;
    }

    /**
     * 将资源图片转换为Drawable对象
     *
     * @param ResId
     * @return
     */
    private Drawable loadDrawable(int ResId) {
        Drawable drawable = getResources().getDrawable(ResId);
        drawable.setBounds(0, 0, drawable.getMinimumWidth() - 5, drawable.getMinimumHeight() - 5);
        return drawable;
    }

    private void initAnimation() {
        anim_out = newAnimation(0, -1);
        anim_in = newAnimation(1, 0);
        anim_in.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                try {
                    updateTipAndPlayAnimationWithCheck();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private Animation newAnimation(float fromYValue, float toYValue) {
        Animation anim = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_SELF, fromYValue, Animation.RELATIVE_TO_SELF, toYValue);
        anim.setDuration(ANIM_DURATION);
        anim.setStartOffset(ANIM_DELAYED_MILLIONS);
        anim.setInterpolator(new DecelerateInterpolator());
        return anim;
    }

    private void updateTipAndPlayAnimationWithCheck() throws JSONException {
        if (System.currentTimeMillis() - lastTimeMillis < 1000) {
            return;
        }
        lastTimeMillis = System.currentTimeMillis();
        updateTipAndPlayAnimation();
    }

    private void updateTipAndPlayAnimation() throws JSONException {
        if (curTipIndex % 2 == 0) {
            updateTip(tv_tip_out);
            tv_tip_in.startAnimation(anim_out);
            tv_tip_out.startAnimation(anim_in);
            this.bringChildToFront(tv_tip_in);
        } else {
            updateTip(tv_tip_in);
            tv_tip_out.startAnimation(anim_out);
            tv_tip_in.startAnimation(anim_in);
            this.bringChildToFront(tv_tip_out);
        }
    }

    private void updateTip(TextView tipView) throws JSONException {
        if (new Random().nextBoolean()) {
            tipView.setCompoundDrawables(null, null, null, null);
        } else {
            tipView.setCompoundDrawables(null, null, null, null);
        }
        String tip = "";
        try {
            tip = getNextTip();
        } catch (Exception e) {
            tip = "恭喜231******7c今日分享东方头条获得0.2元奖励";
        }

        if (!TextUtils.isEmpty(tip)) {
            tipView.setText(tip);
        }
    }

    /**
     * 获取下一条消息
     *
     * @return
     */
    private String getNextTip() throws JSONException {


        if (SessionSingleton.getInstance().marqueeArray.length() == 0) return null;
        return SessionSingleton.getInstance().marqueeArray.getString(curTipIndex++ % SessionSingleton.getInstance().marqueeArray.length());
    }

    public static boolean isListEmpty(JSONArray list) {
        return list == null || list.length() == 0;
    }

    public void setTipList(JSONArray tipList) throws JSONException {
        this.tipList = SessionSingleton.getInstance().marqueeArray;
        curTipIndex = 0;
        updateTip(tv_tip_out);
        updateTipAndPlayAnimation();
    }


}
