package com.lt.flowwall.tools;

import android.content.Context;
import android.content.Intent;

import com.lt.flowwall.activity.YunBuNavigateActivity;
import com.lt.flowwall.utils.HttpUtils;
import com.lt.flowwall.utils.SessionSingleton;
import com.lt.flowwall.utils.Utils;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class YunBuFolwWall {
    private static Context context;


    /**
     * 初始化SDK
     *
     * @param ctx
     */
    public static void init(Context ctx, String channelAccount, String chanelUserAccount, String deviceId, String oaId) {
        context = ctx;


        SessionSingleton.getInstance().ctx = ctx;
        initData(channelAccount, chanelUserAccount, deviceId, oaId);

    }

    /**
     * 初始化SDK
     *
     * @param ctx
     */
    public static void init(Context ctx, String channelAccount, String chanelUserAccount, String deviceId) {
        context = ctx;

        SessionSingleton.getInstance().ctx = ctx;
        initData(channelAccount, chanelUserAccount, deviceId, "none");

    }

    /**
     * 登录
     */
    public static void login() {
        context.startActivity(new Intent(context, YunBuNavigateActivity.class));

    }


    /**
     * initconfig
     */
    public static void initStyleConfig(YBStyleConfig YBStyleConfig) {
        SessionSingleton.getInstance().setStyleConfig(YBStyleConfig);
        SessionSingleton.getInstance().hasStyleConfig = 1;
    }

    public static void initData(String channelAccount, String chanelUserAccount, String deviceId, String oaId) {
        Map<String, String> params = new HashMap<>();
        params.put("channelAccount", channelAccount);
        params.put("chanelUserAccount", chanelUserAccount);
        params.put("deviceId", deviceId);
        params.put("oaId", oaId);
        HttpUtils.doHttpReqeust("POST", SessionSingleton.getInstance().requestBaseUrl + "channelSdkNewInit?", params, new HttpUtils.StringCallback() {

            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject returnJSONObject = new JSONObject(response);
                    if (returnJSONObject.getString("status").equals("success")) {
                        SessionSingleton.getInstance().AccountSingle = returnJSONObject.getJSONObject("userData");
                        SessionSingleton.getInstance().WelfareSingle = returnJSONObject.getJSONObject("userExData");

                        SessionSingleton.getInstance().limitGame = returnJSONObject.getJSONObject("userData").getString("limitGame");

                        SessionSingleton.getInstance().yunbuGameMoneyScale = returnJSONObject.getJSONObject("userData").getDouble("yunbuGameMoneyScale");
                        SessionSingleton.getInstance().yunbuGameChangeScale = returnJSONObject.getJSONObject("userData").getDouble("yunbuGameChangeScale");
                        SessionSingleton.getInstance().yunbuGameGradeSerailMoneyScale = returnJSONObject.getJSONObject("userData").getDouble("yunbuGameGradeSerailMoneyScale");

                        SessionSingleton.getInstance().moneyScaleGameRecharge = returnJSONObject.getJSONObject("userData").getDouble("moneyScaleGameRecharge");
                        SessionSingleton.getInstance().moneyScale = returnJSONObject.getJSONObject("userData").getDouble("moneyScale");

                        SessionSingleton.getInstance().moneyScaleReward = returnJSONObject.getJSONObject("userData").getDouble("moneyScaleReward");

                        SessionSingleton.getInstance().yunbuXianWanMoneySpecialHandel = returnJSONObject.getJSONObject("userData").getDouble("yunbuXianWanMoneySpecialHandel");

                        if (SessionSingleton.getInstance().AccountSingle.has("noShowFinishRewardTaskId")) {
                            SessionSingleton.getInstance().noShowFinishRewardTaskId = returnJSONObject.getJSONObject("userData").getString("noShowFinishRewardTaskId");
                        } else {
                            SessionSingleton.getInstance().noShowFinishRewardTaskId = "none";
                        }

                        SessionSingleton.getInstance().showInsertAd = SessionSingleton.getInstance().AccountSingle.getString("showInsertAd");

                        SessionSingleton.getInstance().chuanshanjia = SessionSingleton.getInstance().AccountSingle.getString("chuanshanjia");
                        SessionSingleton.getInstance().guangdiantong = SessionSingleton.getInstance().AccountSingle.getString("guangdiantong");
                        SessionSingleton.getInstance().kuaishou = SessionSingleton.getInstance().AccountSingle.getString("kuaishou");
                        String shanyou = SessionSingleton.getInstance().AccountSingle.getString("shanyouTaskKey");
                        String mogu = SessionSingleton.getInstance().AccountSingle.getString("easyTaskKey");

                        if (!shanyou.equals("none")) {
                            String[] sy = shanyou.split("\\|");
                            SessionSingleton.getInstance().shanyouId = sy[0];
                            SessionSingleton.getInstance().shanyouSecret = sy[1];
                        }

                        if (!mogu.equals("none")) {
                            String[] mg = mogu.split("\\|");
                            SessionSingleton.getInstance().moguId = mg[0];
                            SessionSingleton.getInstance().moguSecret = mg[1];
                        }

                        if (!SessionSingleton.getInstance().chuanshanjia.equals("none")) {
                            String[] csj = SessionSingleton.getInstance().chuanshanjia.split("\\|");
                            SessionSingleton.getInstance().csjid = csj[0];
                            SessionSingleton.getInstance().csjcp = csj[1];
                            SessionSingleton.getInstance().csjjl = csj[2];
                            SessionSingleton.getInstance().csjbanner = csj[3];
                        }

                        if (!SessionSingleton.getInstance().guangdiantong.equals("none")) {
                            String[] gdt = SessionSingleton.getInstance().guangdiantong.split("\\|");
                            SessionSingleton.getInstance().gdtId = gdt[0];
                            SessionSingleton.getInstance().gdtcp = gdt[1];
                            SessionSingleton.getInstance().gdtjl = gdt[2];
                            SessionSingleton.getInstance().gdtbanner = gdt[3];
                        }

                        if (!SessionSingleton.getInstance().kuaishou.equals("none")) {
                            String[] ks = SessionSingleton.getInstance().kuaishou.split("\\|");
                            SessionSingleton.getInstance().ksId = ks[0];
                            SessionSingleton.getInstance().kscp = ks[1];
                            SessionSingleton.getInstance().ksjl = ks[2];
                            SessionSingleton.getInstance().ksbanner = ks[3];
                        }

                        JSONArray array = returnJSONObject.getJSONObject("userData").getJSONArray("gameChannelSheetArray");
                        for (int i = 0; i < array.length(); i++) {
                            if (array.getJSONObject(i).getString("channelName").equals("嘻趣")) {
                                SessionSingleton.getInstance().XiQuSingle.put("serverQQ", array.getJSONObject(i).getString("serverQQ"));
                                SessionSingleton.getInstance().XiQuSingle.put("courseUrl", array.getJSONObject(i).getString("courseUrl"));
                                String[] key = array.getJSONObject(i).getString("registerKey").split("\\|");
                                SessionSingleton.getInstance().XiQuSingle.put("key", key[0]);
                                SessionSingleton.getInstance().XiQuSingle.put("secret", key[1]);
                            } else if (array.getJSONObject(i).getString("channelName").equals("聚享游")) {
                                SessionSingleton.getInstance().JuXiangWanSingle.put("serverQQ", array.getJSONObject(i).getString("serverQQ"));
                                SessionSingleton.getInstance().JuXiangWanSingle.put("courseUrl", array.getJSONObject(i).getString("courseUrl"));
                                String[] key = array.getJSONObject(i).getString("registerKey").split("\\|");
                                SessionSingleton.getInstance().JuXiangWanSingle.put("key", key[0]);
                                SessionSingleton.getInstance().JuXiangWanSingle.put("secret", key[1]);
                            } else if (array.getJSONObject(i).getString("channelName").equals("多游")) {
                                SessionSingleton.getInstance().DuoYouSingle.put("serverQQ", array.getJSONObject(i).getString("serverQQ"));
                                SessionSingleton.getInstance().DuoYouSingle.put("courseUrl", array.getJSONObject(i).getString("courseUrl"));
                                String[] key = array.getJSONObject(i).getString("registerKey").split("\\|");
                                SessionSingleton.getInstance().DuoYouSingle.put("key", key[0]);
                                SessionSingleton.getInstance().DuoYouSingle.put("secret", key[1]);
                            } else if (array.getJSONObject(i).getString("channelName").equals("闲玩")) {
                                SessionSingleton.getInstance().XianWanSingle.put("serverQQ", array.getJSONObject(i).getString("serverQQ"));
                                SessionSingleton.getInstance().XianWanSingle.put("courseUrl", array.getJSONObject(i).getString("courseUrl"));
                                String[] key = array.getJSONObject(i).getString("registerKey").split("\\|");
                                SessionSingleton.getInstance().XianWanSingle.put("key", key[0]);
                                SessionSingleton.getInstance().XianWanSingle.put("secret", key[1]);
                            }
                        }


                    } else {
                        Utils.showToast(context, returnJSONObject.getString("msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFaileure(int code, Exception e) {
                e.printStackTrace();
            }
        });
    }
}
